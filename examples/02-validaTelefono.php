#!/usr/bin/php
<?php
include("../autoload.php");

$telefono= '899 871 1722';

echo "\n\nTelefono: ". $telefono;
$pac= new siteckPAC();

$valid= $pac->isValidTelefono($telefono);

if( !$valid ) {
	echo "\nTelefono invalido...";
}
else {
	$newTel= $pac->getValidTelefono($telefono);
	echo "\nTelefono valido: ". $newTel. "...";
}

echo "\n\nFin del programa...\n\n";
exit(0);
?>