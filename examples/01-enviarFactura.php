#!/usr/bin/php
<?php
include("../autoload.php");

$user='844084-1-504061';
$pass='pruebapruebapruebapruebapruebapruebapruebapruebapruebapruebapruebapruebapruebapruebapruebapruebapruebapruebapruebaprueba';
$apiKey='111111111111111111111';
$qrCode='CEA4A5457603B609E05349D1950A8972CEA4A5457604B609E05349D1950A8972CEA4A5457605B609E05349D1950A8972CEA4A5457606B609E05349D1950A8972';
$certificado= base64_encode(file_get_contents('../resources/certificados/certificado_kit.cer'));

$datos=array(
	"PAC"=>array(
		"usuario"=>"844084-1-504061", 
		"pass"=>$pass, 
		"produccion"=>"NO", 
		"QR"=>$qrCode
	), 
	"conf"=>array(
		"cer"=>$certificado, 
		"pass"=>"84665c168490988d33d4b1ded2f5edf5b4957784498"
	),
	"rFE"=>array(
		"dVerForm"=>"1.00", 
		"gDGen"=>array(
			"iAmb"=>"2", 
			"iTpEmis"=>"01", 
			"iDoc"=>"01",
			"dNroDF"=>"0352216558", 
			"dPtoFacDF"=>"002", 
			"dSeg"=>"451540101", 
			"dFechaEm"=>"AUTO", 
			"iNatOp"=>"01",
			"iTipoOp"=>"1", 
			"iDest"=>"1",
			"iFormCAFE"=>"1", 
			"iEntCAFE"=>"1", 
			"dEnvFE"=>"1", 
			"iProGen"=>"1", 
			"iTipoTranVenta"=>"1", 
			"gEmis"=>array(
				"gRucEmi"=>array(
					"dTipoRuc"=>"2", 
					"dRuc"=>"844084-1-504061", 
					"dDV"=>"00"
				), 
				"dNombEm"=>"FE generada en ambiente de pruebas - sin valor comercial ni fiscal",
				"dSucEm"=>"0000", 
				"dCoordEm"=>"8.98731,-79.52158", 
				"dDirecEm"=>"domicilio del emisor", 
				"gUbiEm"=>array(
					"dCodUbi"=>"8-8-22", 
					"dCorreg"=>"Ernesto Cordoba Campos", 
					"dDistr"=>"PANAMA", 
					"dProv"=>"PANAMA"
				), 
				"dTfnEm"=>array("0"=>"123-4567"), 
				"dCorElectEmi"=>array("0"=>"demo@siteck.com.mx")
			), 
			"gDatRec"=>array(
				"iTipoRec"=>"01",
				"gRucRec"=>array(
					"dTipoRuc"=>"2", 
					"dRuc"=>"155642124-2-2016", 
					"dDV"=>"95"
				), 
				"dNombRec"=>"FE generada en ambiente de pruebas - sin valor comercial ni fiscal", 
				"dDirecRec"=>"domicilio receptor", 
				"gUbiRec"=>array(
					"dCodUbi"=>"8-8-12", 
					"dCorreg"=>"Juan Diaz",
					"dDistr"=>"PANAMA", 
					"dProv"=>"PANAMA"
				),  
				"cPaisRec"=>"PA"
			)
		), 
		"gItem"=>array(
			"0"=>array(
				"dSecItem"=>"0001", 
				"dDescProd"=>"Ivan es una Nenita 2", 
				"dCodProd"=>"F12345", 
				"dCantCodInt"=>"1.000", 
				"dCodCPBSabr"=>"14",
				"gPrecios"=>array(
					"dPrUnit"=>"0.02", 
					"dPrItem"=>"0.02", 
					"dValTotItem"=>"0.0214"
				), 
				"gITBMSItem"=>array(
					"dTasaITBMS"=>"01", 
					"dValITBMS"=>"0.0014", 
				)
			)
		), 
		"gTot"=>array(
			"dTotNeto"=>"0.02", 
			"dTotITBMS"=>"0.00", 
			"dTotGravado"=>"0.00", 
			"dVTot"=>"0.02", 
			"dTotRec"=>"0.02",
			"iPzPag"=>"2",
			"dNroItems"=>"001", 
			"dVTotItems"=>"0.02", 
			"gFormaPago"=>array(
				"0"=>array(
					"iFormaPago"=>"01", 
					"dVlrCuota"=>"0.02"
				)
			), 
			"gPagPlazo"=>array(
				"0"=>array(
					"dSecItem"=>"1", 
					"dFecItPlazo"=>"2022-10-06T22:10:39-05:00", 
					"dValItPlazo"=>"0.02"
				)
			)
		)
	)
);

$pac= new siteckPAC();
$pac->setSandbox(true); // habilitamos pruebas
$pac->setUser($user);
$pac->setPass($pass);
$pac->setApiKey($apiKey);
$pac->setQrCode($qrCode);
$pac->setData($datos);
$pac->sendToSiteck();

echo "\n\n## Request\n";
print_r($pac->getHeaderRequest());
print_r($pac->getData("json"));

echo "\n\n## Response\n";
print_r($pac->getHeaderResponse());

if( $pac->getError() ) {
	echo "\n\n## Error...\n\n";
	print_r($pac->getError());
	echo "\n";

	if( $pac->getRespuesta() ) { # recuperacion de documento
		print_r($pac->getRespuesta());
	}
}
else {
	$r= $pac->getRespuesta();
	echo "\n\nExito....\n\n";
	if( !$r["xml"] ) {
		echo "\n[*] Problemas para obtener XML...";
	}
	else {
		$fileName= time();
		$fileXml= 'tmp/'. $fileName. '.xml';
		$fileXmlAuth= 'tmp/'. $fileName. '_auth.xml';
		$fp= fopen($fileXml, "w");
		fwrite($fp, base64_decode($r["xml"]));
		fclose($fp);
		unset($fp);
		$fp= fopen($fileXmlAuth, "w");
		fwrite($fp, base64_decode($r["xml_aut"]));
		fclose($fp);
		unset($fp);

		echo "\n[*] XML: ". $fileXml;
		echo "\n[*] XML Auth: ". $fileXmlAuth;
		echo "\n[*] XML: ". $r["cufe"];
		echo "\n\n";
	}
}

echo "\n\nFin del programa...\n\n";
exit(0);
?>
