<?php
/**
* siteckPAC - siteck PAC Panama
*
* libreria para comunicacion con PAC Siteck
*
* @version 1.0
* @author Angel Haniel Cantu Jauregui
* @author angel.cantu@moneybox.business
* @author https://www.moneybox.business
* 
*/
class siteckPAC {
	const API='https://pruebasws.siteck.mx/api/';
	const API_SANDBOX='https://pruebasws.siteck.mx/api/';

	private $sandbox=false;
	private $error=NULL;
	private $error_code=NULL;
	private $sucess=NULL;
	private $sucess_code=NULL;
	private $headerRequest=NULL;
	private $headerResponse=NULL;
	private $user=NULL;
	private $pass=NULL;
	private $apiKey=NULL;
	private $qrCode=NULL;
	private $auCode=NULL;
	private $urlDgi=NULL;
	private $firma=NULL;
	private $dataRaw=NULL;
	private $dataJson=NULL;
	private $uuid=NULL;
	private $fecha=NULL;

	/**
	* establee el estados de sandbox
	* 
	* @param boolean $a verdadero o falso
	*/
	public function setSandbox($a=false) {
		$this->sandbox= $a;
	}

	/**
	* verifica si Sandbox esta activo
	* 
	* @return boolean verdadero o falso
	*/
	public function isSandboxActive() {
		return $this->sandbox;
	}

	/**
	* Establece el mensaje de error
	* 
	* @param string $a mensaje de error
	* @param string $b codigo de error
	*/
	public function setError($a=NULL, $b=NULL) {
		$this->error= ($a ? $a:NULL);
		$this->error_code= ($b ? $b:NULL);
	}

	/**
	* Establece el mensaje de exito
	* 
	* @param string $a mensaje de exito
	* @param string $b codigo de exito
	*/
	public function setSucess($a=NULL, $b=NULL) {
		$this->sucess= $a;
		$this->sucess_code= $b;
	}

	/**
	* Retorna el mensaje de exito
	* 
	* @return string mensaje de exito con codigo
	*/
	public function getSucess() {
		if( is_array($this->sucess) && count($this->sucess) ) {
			return $this->sucess;
		}
		else {
			return ($this->sucess_code ? $this->sucess_code.': ':'').$this->sucess;
		}
	}

	/**
	* Retorna el mensaje de exito
	* 
	* @return string mensaje de exito con codigo
	*/
	public function getRespuesta() {
		return $this->getSucess();
	}

	/**
	* Retorna contenido de la variable $headerRequest
	*
	* @return string contenido de variable
	*/
	public function getHeaderRequest() {
		return $this->headerRequest;
	}

	/**
	* Establece el contenido del Request
	*
	* @param string contenido de variable
	*/
	public function setHeaderRequest($a=NULL) {
		$this->headerRequest= ($a ? $a:NULL);
	}

	/**
	* Retorna contenido de la variable $headerResponse
	*
	* @return string contenido de variable
	*/
	public function getHeaderResponse() {
		return $this->headerResponse;
	}

	/**
	* Establece el contenido del Response
	*
	* @param string contenido de variable
	*/
	public function setHeaderResponse($a=NULL) {
		$this->headerResponse= ($a ? $a:NULL);
	}

	/**
	* Retorna el mensaje de error
	* 
	* @return string mensaje de error con codigo
	*/
	public function getError() {
		return ($this->error_code ? $this->error_code.': ':'').$this->error;
	}

	/**
	* Establece nombre de usuario
	* 
	* @param string $a el usuario
	*/
	public function setUser($a=NULL) {
		$this->user= ($a ? $a:NULL);
	}

	/**
	* Devuelve nombre de usuario
	* 
	* @return string el usuario
	*/
	public function getUser() {
		return $this->user;
	}

	/**
	* Establece apiKey
	* 
	* @param string $a el apiKey
	*/
	public function setApiKey($a=NULL) {
		$this->apiKey= ($a ? $a:NULL);
	}

	/**
	* Devuelve el apiKey
	* 
	* @return string el apiKey
	*/
	public function getApiKey() {
		return $this->apiKey;
	}

	/**
	* Establece codigo QR
	* 
	* @param string $a el codigo qr
	*/
	public function setQrCode($a=NULL) {
		$this->qrCode= ($a ? $a:NULL);
	}

	/**
	* Devuelve el codigo qr
	* 
	* @return string el codigo qr
	*/
	public function getQrCode() {
		return $this->qrCode;
	}

	/**
	* Establece codigo de autorizacion AU
	* 
	* @param string $a el codigo au
	*/
	public function setAU($a=NULL) {
		$this->auCode= ($a ? $a:NULL);
	}

	/**
	* Devuelve el codigo de autorizacion AU
	* 
	* @return string el codigo au
	*/
	public function getAU() {
		return $this->auCode;
	}

	/**
	* Establece codigo de autorizacion AU
	* 
	* @param string $a el codigo au
	*/
	public function setUrlDGI($a=NULL) {
		$this->urlDgi= ($a ? $a:NULL);
	}

	/**
	* Devuelve el codigo de autorizacion AU
	* 
	* @return string el codigo au
	*/
	public function getUrlDGI() {
		return $this->urlDgi;
	}

	/**
	* Establece fecha de timbrado
	* 
	* @param string $a fecha timbrado imprimible
	*/
	public function setFecha($a=NULL) {
		$this->fecha= (strstr($a, 'T') ? strtotime($a):$a);
	}

	/**
	* Devuelve fecha de timbrado
	* 
	* @return string la fecha de timbrado imprimible
	*/
	public function getFecha($format="time") {
		if( !strcmp($format, "linux") )
			return $this->fecha;
		else if( !strcmp($format, "time") )
			return date("Y-m-d, H:i:s", $this->fecha);
		else if( !strcmp($format, "time_esp") )
			return date("d\/m\/Y, H:i:s", $this->fecha);
	}

	/**
	* Establece password
	* 
	* @param string $a el password
	*/
	public function setPass($a=NULL) {
		$this->pass= ($a ? $a:NULL);
	}

	/**
	* Devuelve el password
	* 
	* @return string el password
	*/
	public function getPass() {
		return $this->pass;
	}

	/**
	* Establece los datos en raw
	* 
	* @param string $a array de datos
	*/
	public function setData($a=NULL) {
		$this->dataRaw= ($a ? $a:NULL);
	}

	/**
	* Devuelve los datos en raw
	* 
	* @param string $modo indica la forma en que devuelve datos: array o json
	* @return string el password
	*/
	public function getData($modo="array") {
		if( !strcmp($modo, "array") )
			return $this->dataRaw;
		else if( !strcmp($modo, "json") )
			return json_encode($this->dataRaw);
	}

	/**
	* Comprueba si existen datos raw
	* 
	* @return boolean verdadero o falso
	*/
	public function existData() {
		return ((is_array($this->dataRaw) && count($this->dataRaw)) ? true:false);
	}

	/**
	* Devuelve los datos en raw
	* 
	* @return string la url del API
	*/
	public function getApiUrl() {
		return ( $this->isSandboxActive() ? self::API_SANDBOX : self::API );
	}

	/**
	* envia al API siteck
	*/
	public function sendToSiteck() {
		if( !$this->getUser() )
			$this->setError("no indico el usuario siteck", "001");
		else if( !$this->getPass() )
			$this->setError("no indico el password siteck", "002");
		else if( !$this->existData() )
			$this->setError("no indico datos para envio", "003");
		else if( !$this->getApiKey() )
			$this->setError("no indico el api key", "004");
		else {
			$r='';

			$curl= curl_init(); # inciamos url
			curl_setopt($curl, CURLOPT_URL, $this->getApiUrl() );
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30 );
			curl_setopt($curl, CURLOPT_TIMEOUT, 30 );
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );
			curl_setopt($curl, CURLINFO_HEADER_OUT, true);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST" );
			curl_setopt($curl, CURLOPT_USERAGENT, "moneyBox");
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE );
			curl_setopt($curl, CURLOPT_HEADER, 1 );
			curl_setopt($curl, CURLOPT_POST, 1 );
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC );
			curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
			
			if( $this->isSandboxActive() ) { // modo pruebas
				$contenido= array( 
					// 'Content-Type: application/json', 
					'APIKEY: '.$this->getApiKey(), 
					'test-test: application/json'
				);

				curl_setopt($curl, CURLOPT_HTTPHEADER, $contenido );
			}

			curl_setopt($curl, CURLOPT_POSTFIELDS, array("json"=>$this->getData("json"), "modo"=>"JSON") );
			curl_setopt($curl, CURLOPT_VERBOSE, TRUE);

			$r= curl_exec($curl);

			$rq= curl_getinfo($curl);
			$this->setHeaderRequest($rq["request_header"]); // request
			$this->setHeaderResponse($r); // response
			curl_close($curl);

			// analizamos
			$this->debugResponse();
		}
	}

	/**
	* analizamos respuesta del servidor y separamoslos datos
	*/
	public function debugResponse() {
		// echo "\n## REQUEST\n";
		// print_r($this->getHeaderRequest());
		// print_r(array("json"=>$this->getData("json"), "modo"=>"JSON"));
		// echo "\n\n## RESPONSE\n";
		// print_r($this->getHeaderResponse());

		$x= explode( "\n", $this->getHeaderResponse());
		$r= json_decode($x[count($x)-1]); # obtenemos json
		$rTxt= json_decode(str_replace( "\n", '', $r->MsgRes), true);

		$errores='';
		foreach( $rTxt as $k=>$v ) {
			$errores .= ($v["extra"] ? "<br>".$v["extra"]:"");
		}

		if( $rTxt[0]["iderror"] ) { // error
			/*
			* iderror:361, codigo:9500 - Clave PAC y RUC no coincide
			* iderror:88, codigo:1002 - Documento duplicado
			*/
			if( !strcmp($rTxt[0]["iderror"], "88") ) { # duplicado, mandar CUFE
				$recUuid= json_decode($r->respuesta_ws);

				$this->setError($rTxt[0]["resultado_validacion"], $rTxt[0]["iderror"].'-'.$rTxt[0]["codigo"]);
				$this->setSucess($rTxt[0]["resultado_validacion"], $recUuid["CUFE"]);
			}
			else {
				$this->setError($rTxt[0]["resultado_validacion"].$errores, $rTxt[0]["iderror"].'-'.$rTxt[0]["codigo"]);
				$this->setSucess(NULL, NULL);
			}
		}
		else if( $r->CodRes && strcmp($r->codigo_mf_numero, "0") ) { // codigos de error
			$codigosError= json_decode($r->MsgRes);
			$txtError=array();
			$txtErrorString='';
			
			foreach( $codigosError as $k=>$v ) {
				$txtError[]= array(
					"codigo"=>$v->codigo, 
					"mensaje"=>$v->resultado_validacion
				);
				$txtErrorString .= "<br>".$v->codigo. " - ". $v->resultado_validacion;
			}

			// $this->setError($txtError);
			$this->setError($txtErrorString);
		}
		else {
			if( !$r->codigo_mf_numero ) { # sin errores de codigo
				$this->setSucess(
					array(
						"msg"=>"documento procesado con exito", 
						"cufe"=>$r->CUFE, 
						"au"=>$r->AU, 
						"xml"=>$r->xml, 
						"xml_aut"=>$r->xml_autorizacion_de_uso, 
						"qr"=>$r->QR, 
						"fecha"=>$r->dFecProc
					)
				);
				$this->setAU($r->AU);
				$this->setUrlDGI($r->QR);
				$this->setUUID($r->CUFE);
				$this->setFecha($r->dFecProc);
			}
		}
	}

	/**
	* establecer el UUID
	* 
	* @param string $a valor del uuid
	*/
	public function setUUID($a=NULL) {
		$this->uuid= ($a ? $a:NULL);
	}

	/**
	* retorna el UUID
	* 
	* @return string valor del uuid
	*/
	public function getUUID($a=NULL) {
		return $this->uuid;
	}

	/**
	* rellena de ceros a la izquierda una cifra
	* 
	* @param string $a numero
	* @param string $pos numero de posiciones deseadas
	* @return string nuevo valor
	*/
	public function setCeros($a=NULL, $pos=NULL) {
		if( !$pos )
			return 0;
		else {
			if( strlen($a)==$pos ) { # misma cantidad de posiciones
				return $a; # devolvemos
			}
			else if( strlen($a)>$pos ) { # supera las posiciones
				return $a; # devolvemos
			}
			else {
				$need= ($pos-(strlen($a))); # calculamos los 0(ceros) que necesitamos
				$zero='';

				for($i=0; $i<$need; $i++ ) {
					$zero .= '0';
				}
				unset($need);

				return $zero.$a;
			}
		}
	}

	/**
	* valida y retorna el codigo de panama compra efectiva
	*
	* @param string $a el codigo numerico
	* @return string/boolean el codigo o falso de no encontrad
	*/
	public function getPanamaCatalogoCompras($a=NULL) {
		if( !$a ) 	return false;
		else {
			$sysMbox= new mBoxService();
			$tagRedis= '_pan';

			if( $sysMbox->redisExist("PANAMA_CLAVEPRODSERV".$tagRedis) ) {
				$arr= $sysMbox->redisGet("PANAMA_CLAVEPRODSERV".$tagRedis); // tomamos de redis
			}
			else {
				// if( consultar_datos_general( "PANAMA_CLAVEPRODSERV", "ID='". proteger_cadena($a). "'", "CLAVE" ) )
				// return consultar_datos_general( "PANAMA_CLAVEPRODSERV", "ID='". proteger_cadena($a). "'", "CLAVE" );
				$cons= consultar_enorden( "PANAMA_CLAVEPRODSERV", "CLAVE ASC" );
				if( mysqli_num_rows($cons) ) {
					while( $buf=mysqli_fetch_array($cons) ) {
						$arr[$buf["CLAVE"]]= $buf["CLAVE"];
					}

					if( count($arr) ) {
						$sysMbox->redisSet("PANAMA_CLAVEPRODSERV".$tagRedis, $arr); // agregamos a redis
					}
				}
			}

			$r=0;
			foreach( $arr as $k=>$v ) {
				if( !$r && !strcmp($k, $a) ) {
					$r=$a;
				}
			}

			$sysMbox->redisClose();
			return $r;
		}
	}

	/**
	*	devuelve un string con las comillas escapadas
	*
	*	@return string cadena segura
	*/
	public function desproteger_cadena_src($a=NULL) {
		return html_entity_decode($a, ENT_QUOTES);
	}

	/**
	*	generarPDF() genera PDF apartir de sus elementos
	*
	*	@return boolean estado de la generacion
	*/
	public function generarPDF($id=NULL, $nc=false) {
		$path_file= 'clientes/'. $_SESSION["SUPERID"]. '/'. ($nc ? 'notacredito':'facturas'). '/'. $id. '.pdf';
		system( '/usr/bin/wkhtmltopdf '. substr($path_file, 0, -4). '.html '. $path_file );	# creamos PDF
		return 1;
	}

	/**
	*	generarHTML() genera HTML apartir de sus elementos
	*
	*	@return boolean estado de la generacion
	*/
	public function generarHTML($id=NULL, $nc=false, $ds=false, $dsnc=false) {
		$html_fact= 'clientes/'. $_SESSION["SUPERID"]. '/'.(($dsnc || $nc) ? 'notacredito':'facturas').'/'. $id. '.html';
		$frm= consultar_datos_general((($dsnc || $nc) ? "NOTA_CREDITO":"FACTURACION"), "ID='". proteger_cadena($id). "'", "FORMATO"); # consultamos formato

		if( !strcmp($frm, "normal") && consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='1'", "HTML" ) )
			$tmp= consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='1'", "HTML" );
		else if( !strcmp($frm, "notacredito") && consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='16'", "HTML" ) )
			$tmp= consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='16'", "HTML" );
		else if( !strcmp($frm, "arrenda") && consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='5'", "HTML" ) )
			$tmp= consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='5'", "HTML" );
		else if( !strcmp($frm, "transporte") && consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='10'", "HTML" ) )
			$tmp= consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='10'", "HTML" );
		else if( !strcmp($frm, "honorarios") && consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='4'", "HTML" ) )
			$tmp= consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='4'", "HTML" );
		else if( get_modulos(array($_SESSION["SUPERID"], "isactive", 4)) && !strcmp($frm, "aduana_comer") && consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='2'", "HTML" ) )
			$tmp= consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='2'", "HTML" );
		else if( get_modulos(array($_SESSION["SUPERID"], "isactive", 5)) && !strcmp($frm, "aduana_gasto") && consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='3'", "HTML" ) )
			$tmp= consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='3'", "HTML" );
		else if( get_modulos(array($_SESSION["SUPERID"], "isactive", 4)) && !strcmp( $frm, "aduana_comer") ) # aduana comercializadora
			$tmp= 'tmp/comercializadora_co.html';
		else if( get_modulos(array($_SESSION["SUPERID"], "isactive", 5)) && !strcmp( $frm, "aduana_gasto") )	# cuenta de gastos
			$tmp= 'tmp/gastos.html';
		else {
			if( $ds ) { # documentosoporte
				if( consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='18'", "HTML" ) )
					$tmp= consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='18'", "HTML" );
				else
					$tmp= 'tmp/doctosoporte_co.html';
			}
			else if( $dsnc ) {
				if( consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='19'", "HTML" ) )
					$tmp= consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && TIPO='19'", "HTML" );
				else
					$tmp= 'tmp/doctosoporte_nc_co.html';
			}
			else
				$tmp= ($nc ? 'tmp/factura_nc_pa.html':'tmp/factura_pa.html');
		}
		
		# consulta del folio para obtener informacion 
		$cons= consultar_con( "FOLIO", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && ID_FACTURA='". $id. "'" );
		$buf= mysqli_fetch_array($cons);
		limpiar($cons);

		# gestor de folios
		$cons1= consultar_con( "GESTION_FOLIOS", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && ID='". $buf["ID_GESTION"]. "'" );
		$gestorFolio= mysqli_fetch_array($cons1);
		limpiar($cons1);

		# consulta a la facturacion 
		$cons2= consultar_con( (($dsnc || $nc) ? "NOTA_CREDITO":"FACTURACION"), "ID_USUARIO='". $_SESSION["SUPERID"]. "' && ID='". $id. "'" );
		$factura= mysqli_fetch_array($cons2);
		limpiar($cons2); 
		# consultar datos del cliente 
		$cons3= consultar_con( "CLIENTES", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && ID='". $factura["ID_CLIENTE"]. "'" );
		$cliente= mysqli_fetch_array($cons3);
		limpiar($cons3);
		$html= file_get_contents($tmp); # obtenemos el stream del HTML
		$elfolio= strcmp($buf["SERIE"], "-") ? $buf["SERIE"]. ' - '. $buf["FOLIO"] : $buf["FOLIO"];
		
		# datos del emisor
		$usr= consultar_con( "USUARIOS", "ID='". $_SESSION["SUPERID"]. "'" );
		$usr_b= mysqli_fetch_array($usr);

		$nitEmisor= $usr_b["RFC"];
		$nitReceptor= $cliente["RFC"];

		$html= preg_replace( '/\[RAZON_SOCIAL\]/', desproteger_cadena($usr_b["EMPRESA"]), $html );
		$html= preg_replace( '/\[RFC\]/', desproteger_cadena($nitEmisor), $html );
		$html= preg_replace( '/\[REGIMEN_FISCAL\]/', get_clienteprofile($usr_b["REGIMEN_FISCAL"], "regimen", 0), $html );
		$html= preg_replace( '/\[DIRECCION\]/', desproteger_cadena($usr_b["CALLE"].($usr_b["NUM_EXT"] ? ' '.$usr_b["NUM_EXT"]:'')), $html );
		$html= preg_replace( '/\[CP\]/', desproteger_cadena($usr_b["CP"]), $html );
		$html= preg_replace( '/\[LOCALIDAD\]/', desproteger_cadena(get_clienteprofile( $usr_b["LOCALIDAD"], "sat_localidades", 0 )), $html );
		$html= preg_replace( '/\[CIUDAD\]/', desproteger_cadena(get_clienteprofile( $usr_b["CIUDAD"], "sat_ciudades", 0 )), $html );
		$html= preg_replace( '/\[ESTADO\]/', desproteger_cadena(get_clienteprofile( $usr_b["ESTADO"], "sat_estados", 0 )), $html );
		if( $usr_b["IMAGEN"] )	$html= preg_replace( '/\[LOGO\]/', '<img src="data:image/jpeg;base64,'. base64_encode(file_get_contents($usr_b["IMAGEN"])). '" border="0">', $html );
		else	$html= preg_replace( '/\[LOGO\]/', '', $html );
		$html= preg_replace( '/\[TITULO\]/', ($nc ? 'Nota de Credito ':'Factura '). $elfolio, $html );
		$html= preg_replace( '/\[TELEFONO\]/', desproteger_cadena(($usr_b["TELEFONO"] ? $usr_b["TELEFONO"]:'')), $html );
		$html= preg_replace( '/\[EMAIL\]/', desproteger_cadena(($usr_b["EMAIL"] ? $usr_b["EMAIL"]:'')), $html );
		limpiar($usr);

		# leyenda personalizada
		if( $usr_b["LEYENDA_PERSONALIZADA"] ) {
			$leyendaP= '<div id="cleandiv"><div class="left" style="width:98%;background:lightgray;padding:1% 1% 1% 1%;text-align:left;margin-bottom:10px;">'. desproteger_cadena($usr_b["LEYENDA_PERSONALIZADA"]). '</div></div>';
			$html= preg_replace( '/\[LEYENDA_PERSONALIZADA\]/', $leyendaP, $html ); # leyenda personalizada
			unset($leyendaP);
		}
		else
			$html= preg_replace( '/\[LEYENDA_PERSONALIZADA\]/', "", $html ); # leyenda personalizada
		unset($usr, $usr_b);
		
		# escribimos folio y serie
		if( $buf["SERIE"] && strcmp($buf["SERIE"], "-") ) # si hay serie o es distinto a '-'
			$serie_folio= $buf["SERIE"]. ' - '. $buf["FOLIO"]; # Seriel - Folio
		else		$serie_folio= $buf["FOLIO"]; # Seriel - Folio
		$html= preg_replace( '/\[FOLIO\]/', $serie_folio, $html );
		
		# escribimos fecha del expedicion documento 
		$html= preg_replace( '/\[FECHA_EMISION\]/', date( "d / m / Y, H:i:s", $factura["FECHA"] ), $html );
		$html= preg_replace( '/\[FECHA_DIA\]/', date( "d", $factura["FECHA"] ), $html );
		$html= preg_replace( '/\[FECHA_MES\]/', date( "m", $factura["FECHA"] ), $html );
		$html= preg_replace( '/\[FECHA_ANO\]/', date( "Y", $factura["FECHA"] ), $html );
		
		# direccion
		$html= preg_replace( '/\[CLIENTE_DIRECCION\]/', desproteger_cadena($cliente["CALLE"].($cliente["NUM_EXT"] ? ' '.$cliente["NUM_EXT"]:'')), $html );
		$html= preg_replace( '/\[CLIENTE_LOCALIDAD\]/', desproteger_cadena(get_clienteprofile( $cliente["LOCALIDAD"], "sat_localidades", 0 )), $html );
		$html= preg_replace( '/\[CLIENTE_TELEFONO\]/', $cliente["TELEFONO"], $html ); # telefono
		$html= preg_replace( '/\[CLIENTE_CIUDAD\]/', desproteger_cadena(get_clienteprofile( $cliente["CIUDAD"], "sat_ciudades", 0 )), $html ); # ciudad
		$html= preg_replace( '/\[CLIENTE_ESTADO\]/', desproteger_cadena(get_clienteprofile( $cliente["ESTADO"], "sat_estados", 0 )), $html ); # estado
		$html= preg_replace( '/\[CLIENTE_PAIS\]/', desproteger_cadena(get_clienteprofile( $cliente["PAIS"], "sat_paises", 0 )), $html ); # pais
		$html= preg_replace( '/\[CLIENTE_MAIL\]/', ($cliente["EMAIL"] ? $this->desproteger_cadena_src($cliente["EMAIL"]):''), $html ); # email
		$html= preg_replace( '/\[CLIENTE_RAZON_SOCIAL\]/', desproteger_cadena($cliente["EMPRESA"]), $html ); # razon social
		$html= preg_replace( '/\[CLIENTE_RFC\]/', desproteger_cadena($nitReceptor), $html ); # rfc

		# arrendatarios
		if( !strcmp($frm, "arrenda") ) { # arrendatarios
			$html= preg_replace( '/\[PREDIAL\]/', ($factura["PREDIAL"] ? desproteger_cadena($factura["PREDIAL"]):''), $html ); # rfc
		}

		# formato del documento 
		$frm= consultar_datos_general((($dsnc || $nc) ? "NOTA_CREDITO":"FACTURACION"), "ID='". proteger_cadena($id). "'", "FORMATO"); # consultamos formato
		
		# impresion de la venta de productos
		$datos= init_conceptos( "hash2arr", $factura["DESCRIPCION"]);
		$desc= explode("|", $factura["DESCUENTO"]);
		$data_concept=''; # buffer
		$importes=0;
		$importes_cero=0;
		$satcps= ($factura["SAT_CPS"] ? hashconvert($factura["SAT_CPS"], "hash2arr"):0); # validamos si introdujo manualmente las claves
		foreach( $satcps as $key=>$val )
			$aux[]= $val;
		$satcps= $aux;
		$tasas=array( "0"=>0, "16"=>0 ); # para aduana comercializadora

		foreach( $datos as $key=>$val ) {
			$porce= ($val["impuesto"] ? 16:0 );
			$pprint= $val["pu"];
			$precio_unitario= (($desc[0]==2) ? ($val["pu"]-$val["desc"]):$val["pu"]);
			$cantidad= $val["cantidad"];
			$unidades= ($val["unidad"] ? $val["unidad"]:1);
			$descrip= desproteger_cadena(urldecode($val["concepto"]));
			$numid= desproteger_cadena($val["ni"] ? $val["ni"]:'--');
			$pd= (is_numeric($val["desc"]) ? $val["desc"]:'--'); # descuento del articulo

			if( !strcmp($frm, "aduana_comer") ) { # si es aduana / comercializadora 
				$clavprod= (!$val["cps"] ? '--':$this->getPanamaCatalogoCompras($val["cps"]));
				$cad_buscar= array( '/\$/is', '/\%/is' );
				$cad_remplazo= array( '&#36;', '&#37;' );
				$tasas[$porce] += $precio_unitario;

				$data_concept .= '<div id="cleandiv">
							<li class="pu centroalign">'. $cantidad. '</li>
							<li class="pu centroalign">'. get_unidadesmedida( array('get_name_html', $unidades) ). '</li>
							<li class="cant">'. $descrip. '</li>
							<li class="pu centroalign">'. $clavprod. '</li>
							<li class="pu centroalign">'. $porce. '</li>
							<li align="right" class="importe">'. mbnumber_format($precio_unitario, 2, '.', ','). '</li>
							</div>';
				$importes += ($porce ? $precio_unitario:0);
				$importes_cero += (!$porce ? $precio_unitario:0);
			}
			else { # normal o arrendatario 
				$clavprod= (!$val["cps"] ? '--':$this->getPanamaCatalogoCompras($val["cps"]));
				$cad_buscar= array( '/\$/is', '/\%/is' );
				$cad_remplazo= array( '&#36;', '&#37;' );
				$descrip= preg_replace( $cad_buscar, $cad_remplazo, $descrip );

				$data_concept .= '<div id="cleandiv">
					<li class="cant">'. $cantidad. '</li>
					<li align="center" class="pu">'. $numid. '</li>
					<li class="concepto">'. $descrip. '</li>
					<li class="unidad">'. $clavprod. '</li>
					<li class="unidad">'. get_unidadesmedida( array('get_name_html', $unidades) ). '</li>
					<li align="right" class="pu">'. mbnumber_format(($pprint ? $pprint:0), 2). '</li>
					<li align="right" class="pu">'. mbnumber_format(($pd ? $pd:0), 2). '</li>
					<li align="right" class="importe">'. mbnumber_format($cantidad*$precio_unitario,2). '</li>
					</div>';

				#if( $nc || $dsnc ) # para NC relacionamos la lista de conceptos de su factura
				#	{
				#	$datarel= init_conceptos( array("foliosrelacionados", "hash2arr"), $factura["ID_FACTURA"] );
				#
				#	foreach($datarel as $keyInfo=>$valInfo )
				#		{
				#		$tmpInfo= consultar_datos_general("FACTURACION", "ID='". $valInfo["id_factura"]. "' && ID_USUARIO='". $_SESSION["SUPERID"]. "'", "DESCRIPCION");
				#		$infoData= init_conceptos("hash2arr", $tmpInfo);
				#
				#		foreach( $infoData as $infX=>$infY )
				#			{
				#			$infoFacturaNC= $infY["cantidad"]. ' | '. $infY["concepto"]. ' | '. mbnumber_format(($infY["pu"]-$infY["desc"]), 2, '.', ''). ' | '. mbnumber_format(($infY["cantidad"]*($infY["pu"]-$infY["desc"])), 2, '.', '');
				#			}
				#
				#		$data_concept .= '<div id="cleandiv">
				#			<li class="cant">&nbsp;</li>
				#			<li align="center" class="pu">&nbsp;</li>
				#			<li class="concepto">'. $infoFacturaNC. '</li>
				#			<li class="unidad">&nbsp;</li>
				#			<li align="right" class="pu">&nbsp;</li>
				#			<li align="right" class="pu">&nbsp;</li>
				#			<li align="right" class="importe">&nbsp;</li>
				#			</div>';
				#		unset($infoData);
				#		}
				#	unset($infoFacturaNC, $datarel);
				#	}
			}
		}
		unset($precio_unitario, $cantidad, $descrip, $unidades);

		# extra impuestos
		if( !$factura["IMPUESTOS_LOCALES"] )
			$html= preg_replace( '/\[EXTRA_IMPUESTOS\]/', '', $html ); # subtotal
		else {
			$patron= '@\[descripcion\|([0-9a-zA-Z.,:+_\-/+\%\s#]{1,})\|valor\|([0-9.\%]{1,})\|tipo\|([0-9]{1,})\|porcentaje\|([0-9.\%]{1,})\|importe\|([0-9.\%]{1,})\]@';
			preg_match_all( $patron, $factura["IMPUESTOS_LOCALES"], $out ); # buscamos patron

			// print_r($out);

			if( !$out[0][0] )
				$html= preg_replace( '/\[EXTRA_IMPUESTOS\]/', '', $html ); # subtotal
			else { # patron encontrado
				$impuestosLocales='';
				foreach( $out[0] as $key=>$val ) {
					if( $out[2][$key] ) {
						$impuestosLocales .= '
						<div id="cleandiv">
							<li class="l">'. get_clienteprofile($out[1][$key], "dian_tributos_nombre", 0). ' '. mbnumber_format($out[4][$key], 2, '.', ''). '%</li>
							<li>'. mbnumber_format($out[2][$key], 2, '.', ','). '</li>
						</div>
						';
					}
				}
				$html= preg_replace( '/\[EXTRA_IMPUESTOS\]/', $impuestosLocales, $html ); # subtotal

				unset($aux);
			}

			foreach( $extraImp as $key=>$val ) {
			}
		}

		# descuento
		if( $desc[1] ) { # si hay descuento aplicable
			$d= '
				<div id="cleandiv">
					<li class="l">Deduccion</li>
					<li>'. mbnumber_format($desc[1], 2, '.', ','). '</li>
				</div>';
			$html= preg_replace( '/\[DESCUENTO\]/', $d, $html ); # subtotal
			/*
			$data_concept .= '<div id="cleandiv">
						<li class="cant"></li>
						<li class="concepto"></li>
						<li class="unidad"></li>
						<li align="right" class="pu"></li>
						<li align="right" class="importe"></li>
						</div>
						<div id="cleandiv">
							<li class="cant"></li>
							<li class="concepto">Descuento aplicable por: '. mbnumber_format($factura["DESCUENTO"], 2, '.', ','). ' MXN</li>
							<li class="unidad"></li>
							<li align="right" class="pu"></li>
							<li align="right" class="importe"></li>
						</div>';
			*/
		}
		else 
			$html= preg_replace( '/\[DESCUENTO\]/', '', $html ); # subtotal


		# si es aduana/comerializadora se calculan Tasas, IVAS y cargos de servicio
		if( (get_modulos(array($_SESSION["SUPERID"], "isactive", 5)) || get_modulos(array($_SESSION["SUPERID"], "isactive", 4))) && (!strcmp($frm, "aduana_comer") || !strcmp($frm, "aduana_gasto")) ) {
			$subtotal=0;
			$base=0;
			$total=0;
			$total_cli=0;
			# numero de pedimento 
			$html= preg_replace( '/\[PEDIMENTO\]/', $factura["PEDIMENTO"], $html ); # pedimento

			if( !strcmp($frm, "aduana_gasto") ) {
				# descripcion del pedimento
				$html= preg_replace( '/\[DETALLES\]/', $this->desproteger_cadena_src($factura["DESCRIPCION"] ? $factura["DESCRIPCION"]:'--'), $html ); # # descripcion
				$gastos_cli='';
							
				# verificando valores extras
				if( $factura["EXTRAS"] ) { # si hay campos extras
					$x= explode(",", $factura["EXTRAS"]);
					$extras=init_conceptos( "hash2arr", $x[0] ); # gastos aduana
					$extras_cli=init_conceptos( "hash2arr", $x[1]); # gastos cliente
				}
				else { # no hay valores 
					$extras=0;
					$extras_cli=0;
				}

				# extras de la aduana/comercializadora
				$gastos_adu='';
				if( $extras ) {
					foreach( $extras as $key=>$val ) {
						$clavprod= (!$val["cps"] ? '--':$this->getPanamaCatalogoCompras($val["cps"]));
						$gastos_adu .= '<div id="cleandiv">
							<li class="concepto">'. $this->desproteger_cadena_src($val["concepto"]). '</li>
							<li class="importe">'. $this->desproteger_cadena_src($clavprod). '</li>
							<li class="importe">'. mbnumber_format(($val["cantidad"]*$val["pu"]),2). '</li>
							</div>';
						unset($clavprod);
					}
					$extra_importes= sumgastos_importe( $extras, "normal"); # sumatoria de importes
					$html= preg_replace( '/\[GASTOS_ADUANA\]/', $gastos_adu, $html ); # gastos de aduana
					unset($ext, $r, $extras);
				}
					
				# valores de total de servicio
				$subtotal += $extra_importes; # suma de impuestos de aduana
				$base=$subtotal; # la base es el subtotal
				$cons_im= consultar_con( "IMPUESTOS", "ID_USUARIO='". proteger_cadena($_SESSION["SUPERID"]). "' && NOMBRE='". proteger_cadena($factura["IMPUESTO_NOMBRE"]). "'" );
				$bufimp= mysqli_fetch_array($cons_im); # obtenemos campos 
				
				# extras del cliente
				if( $extras_cli ) {
					foreach( $extras_cli as $key=>$val ) {
						$clavprod= (!$val["cps"] ? '--':$this->getPanamaCatalogoCompras($val["cps"]));
						$gastos_cli .= '<div id="cleandiv">
							<li class="concepto">'. desproteger_cadena($val["concepto"]). '</li>
							<li class="importe">'. $this->desproteger_cadena_src($clavprod). '</li>
							<li class="importe">'. mbnumber_format(($val["cantidad"]*$val["pu"]),2). '</li>
							</div>';
						unset($clavprod);
					}
					$html= preg_replace( '/\[GASTOS_CLIENTE\]/', $gastos_cli, $html ); # gastos de cliente
					$extra_importes2= sumgastos_importe( $extras_cli, "normal" ); # sumatoria
					unset($ext, $r, $extras_cli);
				}
				$total_cli= $extra_importes2;
				$total= $factura["TOTAL"];
				
				# imprimiendo valores
				#
				# iva
				$impuesto= $factura["IMPUESTO"];
				$html= preg_replace( '/\[IVA\]/', mbnumber_format( $impuesto, 2, '.', ','), $html ); # IVA

				# iva letra
				$imp_letra= explode( " ", $factura["IMPUESTO_NOMBRE"] );
				$html= preg_replace( '/\[IVA_P\]/', $imp_letra[1], $html ); # IVA Porcentaje
				
				$html= preg_replace( '/\[SUBTOTAL\]/', mbnumber_format($subtotal, 2, '.', ','), $html ); # subtotal
				$html= preg_replace( '/\[TOTAL_SERVICIOS\]/', mbnumber_format( ($subtotal+$impuesto), 2, '.', ','), $html ); # subtotal
				$html= preg_replace( '/\[TOTAL\]/', mbnumber_format( $total, 2, '.', ','), $html ); # subtotal
				unset($x_sub);
				$html= preg_replace( '/\[TOTAL_GASTOS\]/', mbnumber_format( $total_cli, 2, '.', ','), $html ); # total gastos
				$html= preg_replace( '/\[TOTAL_NETO\]/', mbnumber_format( $total, 2, '.', ','), $html ); # total neto
				$letratotal= convertir_a_letras( $total, $factura["MONEDA_TIPO"] );
				$html= preg_replace( '/\[CANTIDAD_LETRA\]/', $letratotal, $html ); # total neto
			}
			else if( !strcmp($frm, "aduana_comer" ) ) {
				for($i=0; $i<5; $i++ ) {
					$data_concept .= '<div id="cleandiv">
							<li class="cant"></li>
							<li align="right" class="pu"></li>
							<li align="right" class="importe"></li>
							</div>';
				}

				# impuestos de comercializados / aduana
				$p= '16';
				if( $factura["EXTRAS"] ) { # si hay campos extras
					$extras= init_conceptos("hash2arr", $factura["EXTRAS"]);
					foreach( $extras as $key=>$val ) {
						$clavprod= (!$val["cps"] ? '--':$this->getPanamaCatalogoCompras($val["cps"]));
						$porc= ($val["impuesto"] ? 16:0);
						$imp= ($val["cantidad"]*$val["pu"]);
						$tasas[$porc] += $imp;

						$data_concept .= '<div id="cleandiv">
							<li class="cant">'. $this->desproteger_cadena_src($val["concepto"]). '</li>
							<li class="unidad">'. $clavprod. '</li>
							<li align="right" class="pu">'. $this->desproteger_cadena_src($porc). '</li>
							<li align="right" class="importe">'. mbnumber_format($imp,2, '.', ','). '</li>
							</div>';
					}
					unset($ext, $r);
				}

				$tasa_neto= '';
				$total=$factura["TOTAL"];
				$subtotal=$factura["SUBTOTAL"];
				$totalsrv= sumgastos_importe( $extras, "normal" );

				foreach( $tasas as $a=>$b ) {
					$tasa_neto .= '
					<div class="cleandiv">
						<li class="l">Tasa '. $a. '%</li>
						<li>'. mbnumber_format($b, 2, '.', ','). '</li>
					</div>';
				}

				$tasa_neto .= '
					<div class="cleandiv">
						<li class="l">SUBTOTAL</li>
						<li>'. mbnumber_format($subtotal, 2, '.', ','). '</li>
					</div>
					<div class="cleandiv">
						<li class="l">I.V.A. '. $p. '%</li>
						<li>'. mbnumber_format($factura["IMPUESTO"], 2, '.', ','). '</li>
					</div>
					<div class="cleandiv">
						<li class="l">TOTAL</li>
						<li>'. mbnumber_format($total, 2, '.', ','). '</li>
					</div>
					<div class="cleandiv">
						<li class="l">TOTAL SERVICIOS</li>
						<li>'. mbnumber_format($totalsrv+$factura["IMPUESTO"], 2, '.', ','). '</li>
					</div>';

				unset( $preval, $aduana, $comercia, $dta, $importe, $p );

				# cantidad con letra
				$letratotal= convertir_a_letras( $total, $factura["MONEDA_TIPO"] );
				$html= preg_replace( '/\[CANTIDAD_LETRA\]/', $letratotal, $html ); # IVA
				
				# neto tasas
				$html= preg_replace( '/\[TASAS_NETO\]/', $tasa_neto, $html ); # total neto de TASAS
			}

			# comercio exterior
			if( $factura["COMERCIO_EXTERIOR"] ) { # si hay etiquetq
				$comerextra_data= array(
					"r"=>$factura["COMEREXTRA_RECEPTOR"], 
					"d"=>get_systeminfo( array("sys"=>"factura", "op"=>"comerext", "cmd"=>"data2arr", "data"=>$factura["COMEREXTRA_DESTINATARIOS"])), 
					"p"=>get_systeminfo( array("sys"=>"factura", "op"=>"comerext", "cmd"=>"data2arr", "data"=>$factura["COMEREXTRA_PROPIETARIOS"])), 
					"m"=>get_systeminfo( array("sys"=>"factura", "op"=>"spcomerext", "cmd"=>"data2arr", "data"=>$factura["COMEREXTRA_MERCANCIAS"])), 
					"o"=>get_systeminfo( array("sys"=>"factura", "op"=>"comerext", "cmd"=>"data2arr", "data"=>$factura["COMEREXTRA_OTROS"]))
				);

				$cmext= '
					<div class="cleandiv">
						<li style="width:100%;margin-top:16px;" class="titulo negro txtblanco centroalign">COMERCIO EXTERIOR</li>
						<li align="center" class="pu titulo negro txtblanco">cant</li>
						<li align="center" class="importe titulo negro txtblanco">NI</li>
						<li align="center" class="importe titulo negro txtblanco">Fraccion</li>
						<li align="center" class="importe titulo negro txtblanco">mercancias</li>
						<li align="center" class="importe titulo negro txtblanco">unidad</li>
						<li align="center" class="importe titulo negro txtblanco">pu</li>
						<li align="center" class="importe titulo negro txtblanco">importe usd</li>
					</div>
					';

				foreach( $comerextra_data["m"] as $key=>$val ) {
					$cmtxtunid= ( (substr($val["unidad"], 0, -1)==0) ? substr($val["unidad"], -1):$val["unidad"]);
					$cmext .= '
					<div id="cleandiv">
						<li align="center" class="pu txtnegro">'. desproteger_cadena($val["cantidad"]). '</li>
						<li align="center" class="importe txtnegro">'. $this->desproteger_cadena_src($val["serie"]). '</li>
						<li align="center" class="importe txtnegro">'. $this->desproteger_cadena_src(get_clienteprofile( $val["concepto"], "aranceles_clave", 0 )). '</li>
						<li align="center" class="importe txtnegro">'. $this->desproteger_cadena_src(get_clienteprofile( $val["concepto"], "aranceles_nombre", 0 )). '</li>
						<li align="center" class="importe txtnegro">'. $this->desproteger_cadena_src(get_unidadesmedida( array("get_name", $cmtxtunid, "comercio_exterior") )). '</li>
						<li align="center" class="importe txtnegro">'. mbnumber_format($val["pu"], 2, '.', ','). '</li>
						<li align="center" class="importe txtnegro">'. mbnumber_format(($val["cantidad"]*$val["pu"]), 2, '.', ''). '</li>
					</div>
						';
				}
				$html= preg_replace( '/\[COMEXTERIOR\]/', $cmext, $html ); # IVA
			}
			else 	
			$html= preg_replace( '/\[COMEXTERIOR\]/', '', $html ); # IVA
		}
		else { # calculos: normal, arrenda, transporte
			# inicializador
			if( get_modulos(array($_SESSION["SUPERID"], "isactive", 3)) && !strcmp($frm, "normal") ) { # factura comun
				$html= preg_replace( '/\[IVA_RET\]/', '', $html ); # iva ret
				$html= preg_replace( '/\[ISR_RET\]/', '', $html ); # isr ret
			}

			# procentaje IVA impreso
			$xivaprint=1;
			if( $xivaprint ) { # si se requiere imprimir
				if( !strcmp($cliente["REGIMEN_FISCAL"], "30") && !strcmp(consultar_datos_general( "USUARIOS", "ID='". $_SESSION["SUPERID"]. "'", "REGIMEN_FISCAL"), "9") )
					$html= preg_replace( '/\[IVA_P\]/', 'NA', $html ); # iva impreso el porcentaje
				else {
					$ivaporcentaje= $factura["IMPUESTO_FORMULA"];
					$html= preg_replace( '/\[IVA_P\]/', $ivaporcentaje, $html ); # iva impreso el porcentaje
					unset($ivaporcentaje);
				}
			}
			unset($xivaprint);
				
			# IMPUESTO IVA, ISR o IEPS
			if( $factura["IMPUESTO"] && strcmp($factura["IMPUESTO"], "0.00") ) { # si hay algun impuesto a cobrar
				# honorarios y persona fisica (causante menor), solo causa ISR
				if( get_modulos(array($_SESSION["SUPERID"], "isactive", 2)) && !strcmp($frm, "honorarios") && !strcmp($cliente["PERFIL"], "1")  ) {
					$html= preg_replace( '/\[IVA\]/', mbnumber_format( (!$descuento ? $factura["IMPUESTO"]:($factura["IMPUESTO"]*$descuento)), 2, '.', ','), $html ); # iva
					$html= preg_replace( '/\[ISR_RET\]/', '', $html ); # isr ret
					$html= preg_replace( '/\[IVA_RET\]/', '', $html ); # iva ret
				}
				else # normal, arrenda, transporte, aduana y comercializadora -- causan IVA
					$html= preg_replace( '/\[IVA\]/', number_format(($factura["IMPUESTO"] ? (!$descuento ? $factura["IMPUESTO"]:($factura["IMPUESTO"]*$descuento)):0), 2), $html ); # iva
			}
			else { # son servicios sin retencion
				$html= preg_replace( '/\[IVA_RET\]/', mbnumber_format( ($factura["RET_IVA"] ? $factura["RET_IVA"]:0), 2, '.', ','), $html ); # iva ret
				$html= preg_replace( '/\[ISR_RET\]/', mbnumber_format( ($factura["RET_ISR"] ? $factura["RET_ISR"]:0), 2, '.', ','), $html ); # iva ret
			}

			# imprecion del TOTAL
			# factura normal a gobierno con retencion del iva
			if( $factura["RET_IVA"] && get_modulos(array($_SESSION["SUPERID"], "isactive", 1)) && !strcmp($cliente["REGIMEN_FISCAL"], "20") ) {
				$totalgobret= $factura["TOTAL"]; # total nuevo
				$html= preg_replace( '/\[TOTAL\]/', number_format($totalgobret, 2), $html ); # total
				unset($totalgobret);
			}
			else if( (get_modulos(array($_SESSION["SUPERID"], "isactive", 2)) || get_modulos(array($_SESSION["SUPERID"], "isactive", 3))) && (!strcmp($frm, "arrenda") || !strcmp($frm, "honorarios")) ) { # arrendamiento y honorarios
				# si el cliente es causante mayor ponemos retenciones
				if( !strcmp( consultar_datos_general("CLIENTES", "ID='". proteger_cadena($factura["ID_CLIENTE"]). "'", "PERFIL"), "2") ) {
					$html= preg_replace( '/\[ISR_RET\]/', mbnumber_format( (($factura["SUBTOTAL"]*10)/100), 2, '.', ','), $html ); # isr ret

					# a gobierno
					if( !strcmp($cliente["REGIMEN_FISCAL"], "20") )
						$html= preg_replace( '/\[TOTAL\]/', mbnumber_format( $factura["TOTAL"], 2, '.', ','), $html ); # total
					else {	# persona moral
						$html= preg_replace( '/\[IVA_RET\]/', mbnumber_format( (($factura["IMPUESTO"]*66.666)/100), 2, '.', ','), $html ); # iva ret
						$html= preg_replace( '/\[TOTAL\]/', mbnumber_format( $factura["TOTAL"], 2, '.', ','), $html ); # total
					}
				}
				else	# imprimimos el "subtotal" como total por disposicion legal
					$html= preg_replace( '/\[TOTAL\]/', mbnumber_format($factura["TOTAL"], 2, '.', ','), $html ); # total
			}
			else if( get_modulos(array($_SESSION["SUPERID"], "isactive", 12)) && !strcmp($frm, "transporte") ) { # transportista
				$html= preg_replace( '/\[IVA_RET\]/', mbnumber_format( $factura["RET_IVA"], 2, '.', ','), $html ); # iva ret
				$html= preg_replace( '/\[TOTAL\]/', mbnumber_format( $factura["TOTAL"], 2, '.', ','), $html ); # total
				$html= preg_replace( '/\[ISR_RET\]/', '', $html ); # isr ret
			}
			else		# factura normal
				$html= preg_replace( '/\[TOTAL\]/', mbnumber_format( (!$descuento ? $factura["TOTAL"]:($factura["TOTAL"]*$descuento)), 2, '.', ','), $html ); # total

			# arrendatarios y honorarios -- calculo de total
			if( (get_modulos(array($_SESSION["SUPERID"], "isactive", 2)) || get_modulos(array($_SESSION["SUPERID"], "isactive", 3))) && (!strcmp($frm, "arrenda") || !strcmp($frm, "honorarios")) ) {
				# causante mayor
				if( !strcmp( consultar_datos_general("CLIENTES", "ID='". proteger_cadena($factura["ID_CLIENTE"]). "'", "PERFIL"), "2") ) {
					# a gobierno
					if( !strcmp($cliente["REGIMEN_FISCAL"], "20") )
						$tl= mbnumber_format(($factura["TOTAL"]+(($factura["SUBTOTAL"]*10)/100)), 2, '.', '');
					else		# persona moral
						$tl= mbnumber_format($factura["TOTAL"], 2, '.', '' );
				}
				else # causante menor
					$tl= mbnumber_format($factura["TOTAL"], 2, '.', '' );
			}
			# factura normal a gobierno con retencion del iva
			else 
				$tl= mbnumber_format((!$descuento ? $factura["TOTAL"]:($factura["TOTAL"]*$descuento)), 2, '.', '' );

			# prevencion para COTIZACIONES
			$html= preg_replace( '/\[IVA\]/', '0.00', $html ); # iva ret
			$html= preg_replace( '/\[IVA_RET\]/', '0.00', $html ); # iva ret
			$html= preg_replace( '/\[ISR_RET\]/', '0.00', $html ); # isr ret

			# COMPLEMENTOS
			#
			# IMPUESTOS LOCALES
			# 

			#if( !$factura["IMPUESTOS_LOCALES"] ) # si no hay impuestos locales
				$html= preg_replace( '/\[COMPLEMENTOS\]/', '', $html ); # dejamos vacio
			#else # sacamos los impuestos locales
			#	{
			#	$implocales_info='';
			#	$implocales= get_systeminfo(array( "sys"=>"factura", "op"=>"implocal", "cmd"=>"data2arr", "data"=>$factura["IMPUESTOS_LOCALES"] )); # consultamos de data a array
			#
			#	foreach( $implocales as $key=>$val )
			#		{
			#		$implocal_val= $val["porcentaje"];
			#		$implocal_imp= $val["valor"];
			#
			#		$implocales_info .= '
			#			<div id="cleandiv">
			#				<li class="l">'. desproteger_cadena($val["descripcion"]). ' '. ( !strcmp($val["tipo"], "1" ) ? 'Ret.':'Tras.'). ' '. $this->desproteger_cadena_src($implocal_val). '</li>
			#				<li>'. mbnumber_format($implocal_imp, 2, '.', ','). '</li>
			#			</div>';
			#		unset($implocal_imp, $implocal_val);
			#		}
			#
			#	$html= preg_replace( '/\[COMPLEMENTOS\]/', $implocales_info, $html ); # agregamos
			#	unset($implocales);
			#	}

			$letratotal= convertir_a_letras( $tl, $factura["MONEDA_TIPO"] );
			$html= preg_replace( '/\[CANTIDAD_LETRA\]/', $letratotal, $html ); # cantidad con letra
			unset($tl);
		}

		# nota de credito
		if( $nc || $dsnc ) {
			$data_amparo='';
			if( $factura["ID_FACTURA"] ) {
				$datarel= init_conceptos( array("foliosrelacionados", "hash2arr"), $factura["ID_FACTURA"] );

				foreach($datarel as $key=>$val ) {
					$data_amparo .= '
					<li style="width:60%;" class="concepto blanco txtnegro centroalign">'. $val["uuid"]. '</li>
					<li style="width:40%;" class="blanco txtnegro centroalign">'. date( "d/m/Y", consultar_datos_general( "FACTURACION", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && ID='". $val["id_factura"]. "'", "FECHA")). '</li>
					';
				}
				unset($datarel);
			}

			$html= preg_replace( '/\[CONCEPTOS_AMPARO\]/', $data_amparo, $html ); # conceptos de factura
			unset($data_amparo);

			# tipo de operacion
			$html= preg_replace( '/\[TIPO_OPERACION\]/', get_clienteprofile($factura["TIPO_OPERACION"], "tipo_operacion", 0), $html ); # conceptos de factura
		}

		#
		# documentos relacionados
		#
		$doctosrel='';
		if( $factura["DOCTOSREL_FACTURAS"] || $factura["DOCTOSREL_CANCELACIONES"] || $factura["DOCTOSREL_NOTASCREDITO"] ) {
			$doctosrel_arr= get_systeminfo(array( "sys"=>"factura", "op"=>"doctosrel", "cmd"=>"data2arr", "data"=>$factura["ID"])); # inicializa
			$doctosrel .= '
			<div id="receptor">
				<h1 class="titulo negro txtblanco">Documentos Relacionados</h1>
					<div class="in">
						<ul>
				';

			# factura
			if( count($doctosrel_arr["doctos"]["factura"]) ) {
				foreach( $doctosrel_arr["doctos"]["factura"] as $key=>$val )
					$doctosrel .= '<div id="cleandiv"><li class="cp">'. $val["nombre"]. '</li><li class="cp">'. strtoupper($val["uuid"]). '</li><li class="cp rightalign">'. mbnumber_format($val["monto"], 2, '.', ','). '</li></div>';
			}
			# cancelaciones
			if( count($doctosrel_arr["doctos"]["cancelaciones"]) ) {
				foreach( $doctosrel_arr["doctos"]["cancelaciones"] as $key=>$val )
					$doctosrel .= '<div id="cleandiv"><li class="cp">Cancelada '. $val["nombre"]. '</li><li class="cp">'. strtoupper($val["uuid"]). '</li><li class="cp rightalign">'. mbnumber_format($val["monto"], 2, '.', ','). '</li></div>';
			}
			# nota de credito
			if( count($doctosrel_arr["doctos"]["notacredito"]) ) {
				foreach( $doctosrel_arr["doctos"]["notacredito"] as $key=>$val )
					$doctosrel .= '<div id="cleandiv"><li class="cp">'. $val["nombre"]. '</li><li class="cp">'. strtoupper($val["uuid"]). '</li><li class="cp rightalign">'. mbnumber_format($val["monto"], 2, '.', ','). '</li></div>';
			}
			$doctosrel .= '
						</ul>
					</div>
				</div>';
		}

		$html= preg_replace( '/\[DOCUMENTOSRELACIONADOS\]/', $doctosrel, $html ); # documentos relacionados

		$html= preg_replace( '/\[SUBTOTAL\]/', mbnumber_format($factura["SUBTOTAL"], 2), $html ); # subtotal	
		$html= preg_replace( '/\[CONCEPTOS\]/', $data_concept, $html ); # conceptos de factura
		unset($frm, $x_sub, $y, $datos);

		# predial - arrendatarios
		$html= preg_replace( '/\[PREDIAL\]/', ($factura["PREDIAL"] ? $factura["PREDIAL"]:''), $html ); # metodo de pago

		# metodo de pago
		# 1 --> efectivo, 2 --> no identificado, 3 --> transferencia, 4 --> tarjeta debito/credito/servicio, 5 --> cheque nominativo
		#
		# si se requieren impresos
		# 
		$metodopago= get_clienteprofile( $factura["METODO_PAGO"], "metodopago", 0 ); # texto del metodo de pago
		#$metodopago .= ((strcmp($factura["METODO_PAGO"], "10") && strcmp($factura["METODO_PAGO"], "1")) ? ', Cuenta: '. desproteger_cadena($factura["METODO_PAGO_DIGITOS"]):'');

		if( $metodopago ) # si hay metodo de pago
			$html= preg_replace( '/\[METODO_PAGO\]/', $metodopago, $html ); # metodo de pago
		unset( $metodopago);

		$fPago='';
		if( date("Y", $factura["FECHA_VENCIMIENTO"])<date("Y", time()) )
			$fPago= 'Contado';
		else if( date("Y", $factura["FECHA_VENCIMIENTO"])==date("Y", time()) ) {
			if( date("m", $factura["FECHA_VENCIMIENTO"])<date("m", time()) )
				$fPago= 'Contado';
			else if( date("m", $factura["FECHA_VENCIMIENTO"])==date("m", time()) ) {
				if( date("d", $factura["FECHA_VENCIMIENTO"])<date("d", time()) )
					$fPago= 'Contado';
				else if( date("d", $factura["FECHA_VENCIMIENTO"])==date("d", time()) )
					$fPago= 'Contado';
				else
					$fPago= 'Cr&eacute;dito';
			}
			else
				$fPago= 'Cr&eacute;dito';
		}
		else
			$fPago= 'Cr&eacute;dito';

		#$html= preg_replace( '/\[FORMA_PAGO\]/', (!$factura["FORMA_PAGO"] ? '':($factura["FECHA_VENCIMIENTO"] ? (!strcmp(date("d/m/Y", $factura["FECHA_VENCIMIENTO"]), date("d/m/Y", time())) ? "Contado":"Cr&eacute;dito"):($factura["FORMA_PAGO"]. ' - '. get_clienteprofile($factura["FORMA_PAGO"], "forma_pago", 0))) ), $html ); # metodo de pago
		$html= preg_replace( '/\[FORMA_PAGO\]/', (!$factura["FORMA_PAGO"] ? '':($factura["FECHA_VENCIMIENTO"] ? $fPago:($factura["FORMA_PAGO"]. ' - '. get_clienteprofile($factura["FORMA_PAGO"], "forma_pago", 0))) ), $html ); # metodo de pago
		unset($fPago);

		$html= preg_replace( '/\[USO_CFD\]/', ($factura["USO_CFD"] ? get_clienteprofile( $factura["USO_CFD"], "usocfd", 0 ):'G03'), $html ); # usocfdi

		#
		# tipo de moneda
		#
		$html= preg_replace( '/\[MONEDA_TIPO\]/', get_clienteprofile( $factura["MONEDA_TIPO"], "moneda", 0 ), $html );
		$html= preg_replace( '/\[MONEDA_VALOR\]/', $factura["MONEDA_VALOR"], $html );
		$html= preg_replace( '/\[CONDICION_PAGO\]/', desproteger_cadena(get_clienteprofile($factura["CONDICION_PAGO"], "condicion_pago", 0) ? get_clienteprofile($factura["CONDICION_PAGO"], "condicion_pago", 0):'Otros'), $html ); # usocfdi

		# leyenda fiscal
		$html= preg_replace( '/\[LEYENDA_FISCAL\]/', ($factura["LEYENDAS_FISCALES"] ? $factura["LEYENDAS_FISCALES"]:''), $html );
		
		#
		# orden de compra, poliza y referencia
		#
		if( $factura["OC"] && strcmp($factura["OC"], "Orden de Compra") )
			$html= preg_replace( '/\[CLIENTE_PO\]/', desproteger_cadena($factura["OC"]), $html ); # orden de compra
		else		$html= preg_replace( '/\[CLIENTE_PO\]/', '', $html ); # orden de compra
		if( $factura["POLIZA"] && strcmp($factura["POLIZA"], "Poliza de Servicio") )
			$html= preg_replace( '/\[CLIENTE_POLIZA\]/', desproteger_cadena($factura["POLIZA"]), $html ); # poliza
		else		$html= preg_replace( '/\[CLIENTE_POLIZA\]/', '', $html ); # poliza
		if( $factura["REF"] && strcmp($factura["REF"], "Referencia") )
			$html= preg_replace( '/\[CLIENTE_REFERENCIA\]/', desproteger_cadena($factura["REF"]), $html ); # referencia
		else		$html= preg_replace( '/\[CLIENTE_REFERENCIA\]/', '', $html ); # referencia

		$html= preg_replace( '/\[FECHA_VENCIMIENTO\]/', ($factura["FECHA_VENCIMIENTO"] ? date( "d/m/Y", $factura["FECHA_VENCIMIENTO"]):"---"), $html );

		# si expide Facturas Digitales 	
		if( !strcmp(consultar_datos_general("USUARIOS", "ID='". proteger_cadena($_SESSION["SUPERID"]). "'", "SERVICIO"), "fd") ) {
			# $html= preg_replace( '/\[SERIE_CSD\]/', $this->getCert(), $html ); # serie csd
			$html= preg_replace( '/\[TIMBRE_FISCAL\]/', $this->getUUID(), $html ); # folio fiscal UUID
			$html= preg_replace( '/\[SELLO_DGI\]/', $this->getAU(), $html ); # sello SAT
			$html= preg_replace( '/\[FECHA_TIMBRE\]/', $this->getFecha(), $html ); # fecha timbre fiscal
			# $html= preg_replace( '/\[CO\]/', $sello, $html ); # cadena original
			
			# url descarga xml y pdf
			if( $factura["SHORT_URL"] )
				$html= preg_replace( '/\[URL\]/', $factura["SHORT_URL"], $html ); # orden de compra
			# url validacion DGI
			$html= preg_replace( '/\[URL_VALIDA_DGI\]/', $this->getUrlDGI(), $html ); # orden de compra
			
			unset($timbredata, $xml, $filet, $rwf, $urlDian, $urlDianSrc);
		}

		# limpiamos variables
		unset($buf, $factura, $cliente);
		$fp= fopen( $html_fact, "w" ); # creamos archivo html
		fwrite($fp, $html );	# guardamos datos html
		fclose($fp);	# cerramos stream
		return 1;
	}

	/**
	*	generarPDF() genera PDF apartir de sus elementos
	*
	* @param string $id identificador de la factura
	* @param string $nc identificador de la nota de credito o false para no incluirla
	* @param string $idUser identificador del usuario a realizarle la accion
	*
	* @return boolean estado de la generacion
	*/
	public function generarPDFfix($id=NULL, $nc=false, $idUser=false) {
		$path_file= 'clientes/'. $idUser. '/'. ($nc ? 'notacredito':'facturas'). '/'. $id. '.pdf';
		system( '/usr/bin/wkhtmltopdf '. substr($path_file, 0, -4). '.html '. $path_file );	# creamos PDF
		return 1;
	}

	/**
	* genera un HTML de seguridad para Soporte tecnico en correcciones de fallo del sistema
	*
	* @param string $id identificador de la factura
	* @param string $nc identificador de la nota de credito o false para no incluirla
	* @param string $idUser identificador del usuario a realizarle la accion
	*/
	public function generarHTMLfix($id=NULL, $nc=false, $idUser=false, $ds=false, $dsnc=false) {
		$html_fact= 'clientes/'. $idUser. '/'.(($dsnc || $nc) ? 'notacredito':'facturas').'/'. $id. '.html';
		$frm= consultar_datos_general((($dsnc || $nc) ? "NOTA_CREDITO":"FACTURACION"), "ID='". proteger_cadena($id). "'", "FORMATO"); # consultamos formato

		if( !strcmp($frm, "normal") && consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $idUser. "' && TIPO='1'", "HTML" ) )
			$tmp= consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $idUser. "' && TIPO='1'", "HTML" );
		else if( !strcmp($frm, "notacredito") && consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $idUser. "' && TIPO='16'", "HTML" ) )
			$tmp= consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $idUser. "' && TIPO='16'", "HTML" );
		else if( !strcmp($frm, "arrenda") && consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $idUser. "' && TIPO='5'", "HTML" ) )
			$tmp= consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $idUser. "' && TIPO='5'", "HTML" );
		else if( !strcmp($frm, "transporte") && consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $idUser. "' && TIPO='10'", "HTML" ) )
			$tmp= consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $idUser. "' && TIPO='10'", "HTML" );
		else if( !strcmp($frm, "honorarios") && consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $idUser. "' && TIPO='4'", "HTML" ) )
			$tmp= consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $idUser. "' && TIPO='4'", "HTML" );
		else if( get_modulos(array($idUser, "isactive", 4)) && !strcmp($frm, "aduana_comer") && consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $idUser. "' && TIPO='2'", "HTML" ) )
			$tmp= consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $idUser. "' && TIPO='2'", "HTML" );
		else if( get_modulos(array($idUser, "isactive", 5)) && !strcmp($frm, "aduana_gasto") && consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $idUser. "' && TIPO='3'", "HTML" ) )
			$tmp= consultar_datos_general( "FORMATOS_DIGITALES", "ID_USUARIO='". $idUser. "' && TIPO='3'", "HTML" );
		else if( get_modulos(array($idUser, "isactive", 4)) && !strcmp( $frm, "aduana_comer") ) # aduana comercializadora
			$tmp= 'tmp/comercializadora_co.html';
		else if( get_modulos(array($idUser, "isactive", 5)) && !strcmp( $frm, "aduana_gasto") )	# cuenta de gastos
			$tmp= 'tmp/gastos.html';
		else {
			if( $ds ) # documentosoporte
				$tmp= 'tmp/doctosoporte_co.html';
			else if( $dsnc )
				$tmp= 'tmp/doctosoporte_nc_co.html';
			else
				$tmp= ($nc ? 'tmp/factura_nc_pa.html':'tmp/factura_pa.html');
		}
		
		# consulta del folio para obtener informacion 
		$cons= consultar_con( "FOLIO", "ID_USUARIO='". $idUser. "' && ID_FACTURA='". $id. "'" );
		$buf= mysqli_fetch_array($cons);
		limpiar($cons);
		# gestor de folios
		$cons1= consultar_con( "GESTION_FOLIOS", "ID_USUARIO='". $_SESSION["SUPERID"]. "' && ID='". $buf["ID_GESTION"]. "'" );
		$gestorFolio= mysqli_fetch_array($cons1);
		limpiar($cons1);
		# consulta a la facturacion 
		$cons2= consultar_con( (($dsnc || $nc) ? "NOTA_CREDITO":"FACTURACION"), "ID_USUARIO='". $idUser. "' && ID='". $id. "'" );
		$factura= mysqli_fetch_array($cons2);
		limpiar($cons2); 
		# consultar datos del cliente 
		$cons3= consultar_con( "CLIENTES", "ID_USUARIO='". $idUser. "' && ID='". $factura["ID_CLIENTE"]. "'" );
		$cliente= mysqli_fetch_array($cons3);
		limpiar($cons3);
		$html= file_get_contents($tmp); # obtenemos el stream del HTML
		$elfolio= strcmp($buf["SERIE"], "-") ? $buf["SERIE"]. ' - '. $buf["FOLIO"] : $buf["FOLIO"];
		
		# datos del emisor
		$usr= consultar_con( "USUARIOS", "ID='". $idUser. "'" );
		$usr_b= mysqli_fetch_array($usr);

		$nitEmisor= $usr_b["RFC"];
		$nitReceptor= $cliente["RFC"];
		
		$html= preg_replace( '/\[RAZON_SOCIAL\]/', desproteger_cadena($usr_b["EMPRESA"]), $html );
		$html= preg_replace( '/\[RFC\]/', desproteger_cadena($nitEmisor), $html );
		$html= preg_replace( '/\[REGIMEN_FISCAL\]/', get_clienteprofile($usr_b["REGIMEN_FISCAL"], "regimen", 0), $html );
		$html= preg_replace( '/\[DIRECCION\]/', desproteger_cadena($usr_b["CALLE"].($usr_b["NUM_EXT"] ? ' '.$usr_b["NUM_EXT"]:'')), $html );
		$html= preg_replace( '/\[CP\]/', desproteger_cadena($usr_b["CP"]), $html );
		$html= preg_replace( '/\[LOCALIDAD\]/', desproteger_cadena(get_clienteprofile( $usr_b["LOCALIDAD"], "sat_localidades", 0 )), $html );
		$html= preg_replace( '/\[CIUDAD\]/', desproteger_cadena(get_clienteprofile( $usr_b["CIUDAD"], "sat_ciudades", 0 )), $html );
		$html= preg_replace( '/\[ESTADO\]/', desproteger_cadena(get_clienteprofile( $usr_b["ESTADO"], "sat_estados", 0 )), $html );
		if( $usr_b["IMAGEN"] )	$html= preg_replace( '/\[LOGO\]/', '<img src="data:image/jpeg;base64,'. base64_encode(file_get_contents($usr_b["IMAGEN"])). '" border="0">', $html );
		else	$html= preg_replace( '/\[LOGO\]/', '', $html );
		$html= preg_replace( '/\[TITULO\]/', ($nc ? 'Nota de Credito ':'Factura '). $elfolio, $html );
		$html= preg_replace( '/\[TELEFONO\]/', desproteger_cadena(($usr_b["TELEFONO"] ? $usr_b["TELEFONO"]:'')), $html );
		$html= preg_replace( '/\[EMAIL\]/', desproteger_cadena(($usr_b["EMAIL"] ? $usr_b["EMAIL"]:'')), $html );
		limpiar($usr);
		unset($usr, $usr_b);
		
		# escribimos folio y serie
		if( $buf["SERIE"] && strcmp($buf["SERIE"], "-") ) # si hay serie o es distinto a '-'
			$serie_folio= $buf["SERIE"]. ' - '. $buf["FOLIO"]; # Seriel - Folio
		else		$serie_folio= $buf["FOLIO"]; # Seriel - Folio
		$html= preg_replace( '/\[FOLIO\]/', $serie_folio, $html );
		
		# escribimos fecha del expedicion documento 
		$html= preg_replace( '/\[FECHA_EMISION\]/', date( "d / m / Y, H:i:s", $factura["FECHA"] ), $html );
		$html= preg_replace( '/\[FECHA_DIA\]/', date( "d", $factura["FECHA"] ), $html );
		$html= preg_replace( '/\[FECHA_MES\]/', date( "m", $factura["FECHA"] ), $html );
		$html= preg_replace( '/\[FECHA_ANO\]/', date( "Y", $factura["FECHA"] ), $html );
		
		# direccion
		$direccion='';
		if( strcmp($cliente["CALLE"], "direccion de calle") && strcmp($cliente["CALLE"], "-") )
			$html= preg_replace( '/\[CLIENTE_CALLE\]/', desproteger_cadena($cliente["CALLE"]), $html );
		if( $cliente["NUM_EXT"] ) { # si tiene datos
			if( !strcmp($cliente["NUM_EXT"], "s/n") )
				$direccion .= ' s/n';
			else if( strcmp($cliente["NUM_EXT"], "-") ) # sin numero
				$direccion .= ' #'. desproteger_cadena($cliente["NUM_EXT"]);
				
			$html= preg_replace( '/\[CLIENTE_NUM_EXT\]/', desproteger_cadena($direccion ? $direccion:''), $html );
		}
		else
			$html= preg_replace( '/\[CLIENTE_NUM_EXT\]/', '', $html );

		$html= preg_replace( '/\[CLIENTE_LOCALIDAD\]/', desproteger_cadena(get_clienteprofile( $cliente["LOCALIDAD"], "sat_localidades", 0 )), $html );
		$html= preg_replace( '/\[CLIENTE_TELEFONO\]/', $cliente["TELEFONO"], $html ); # telefono
		$html= preg_replace( '/\[CLIENTE_CIUDAD\]/', desproteger_cadena(get_clienteprofile( $cliente["CIUDAD"], "sat_ciudades", 0 )), $html ); # ciudad
		$html= preg_replace( '/\[CLIENTE_ESTADO\]/', desproteger_cadena(get_clienteprofile( $cliente["ESTADO"], "sat_estados", 0 )), $html ); # estado
		$html= preg_replace( '/\[CLIENTE_PAIS\]/', desproteger_cadena(get_clienteprofile( $cliente["PAIS"], "sat_paises", 0 )), $html ); # pais
		$html= preg_replace( '/\[CLIENTE_MAIL\]/', ($cliente["EMAIL"] ? $this->desproteger_cadena_src($cliente["EMAIL"]):''), $html ); # email
		$html= preg_replace( '/\[CLIENTE_RAZON_SOCIAL\]/', desproteger_cadena($cliente["EMPRESA"]), $html ); # razon social
		$html= preg_replace( '/\[CLIENTE_RFC\]/', desproteger_cadena($nitReceptor), $html ); # rfc

		# leyenda personalizada
		if( $usr_b["LEYENDA_PERSONALIZADA"] ) {
			$leyendaP= '<div id="cleandiv"><div class="left" style="width:98%;background:lightgray;padding:1% 1% 1% 1%;text-align:left;margin-bottom:10px;">'. desproteger_cadena($usr_b["LEYENDA_PERSONALIZADA"]). '</div></div>';
			$html= preg_replace( '/\[LEYENDA_PERSONALIZADA\]/', desproteger_cadena($leyendaP), $html ); # leyenda personalizada
			unset($leyendaP);
		}
		else
			$html= preg_replace( '/\[LEYENDA_PERSONALIZADA\]/', "", $html ); # leyenda personalizada

		# arrendatarios
		if( !strcmp($frm, "arrenda") ) { # arrendatarios
			$html= preg_replace( '/\[PREDIAL\]/', ($factura["PREDIAL"] ? desproteger_cadena($factura["PREDIAL"]):''), $html ); # rfc
		}

		# formato del documento 
		$frm= consultar_datos_general((($dsnc || $nc) ? "NOTA_CREDITO":"FACTURACION"), "ID='". proteger_cadena($id). "'", "FORMATO"); # consultamos formato
		
		# impresion de la venta de productos
		$datos= init_conceptos( "hash2arr", $factura["DESCRIPCION"]);
		$desc= explode("|", $factura["DESCUENTO"]);
		$data_concept=''; # buffer
		$importes=0;
		$importes_cero=0;
		$satcps= ($factura["SAT_CPS"] ? hashconvert($factura["SAT_CPS"], "hash2arr"):0); # validamos si introdujo manualmente las claves
		foreach( $satcps as $key=>$val )
			$aux[]= $val;
		$satcps= $aux;
		$tasas=array( "0"=>0, "16"=>0 ); # para aduana comercializadora

		foreach( $datos as $key=>$val ) {
			$porce= ($val["impuesto"] ? 16:0 );
			$pprint= $val["pu"];
			$precio_unitario= (($desc[0]==2) ? ($val["pu"]-$val["desc"]):$val["pu"]);
			$cantidad= $val["cantidad"];
			$unidades= $val["unidad"];
			$descrip= desproteger_cadena(urldecode($val["concepto"]));
			$numid= desproteger_cadena($val["ni"] ? $val["ni"]:'--');
			$pd= (is_numeric($val["desc"]) ? $val["desc"]:'--'); # descuento del articulo

			if( !strcmp($frm, "aduana_comer") ) { # si es aduana / comercializadora 
				$clavprod= (!$val["cps"] ? '--':$this->getPanamaCatalogoCompras($val["cps"]));
				$cad_buscar= array( '/\$/is', '/\%/is' );
				$cad_remplazo= array( '&#36;', '&#37;' );
				$tasas[$porce] += $precio_unitario;

				$data_concept .= '<div id="cleandiv">
							<li class="pu centroalign">'. $cantidad. '</li>
							<li class="pu centroalign">'. get_unidadesmedida( array('get_name_html', $unidades) ). '</li>
							<li class="cant">'. $descrip. '</li>
							<li class="pu centroalign">'. $clavprod. '</li>
							<li class="pu centroalign">'. $porce. '</li>
							<li align="right" class="importe">'. mbnumber_format($precio_unitario, 2, '.', ','). '</li>
							</div>';
				$importes += ($porce ? $precio_unitario:0);
				$importes_cero += (!$porce ? $precio_unitario:0);
			}
			else { # normal o arrendatario 
				$clavprod= (!$val["cps"] ? '--':$this->getPanamaCatalogoCompras($val["cps"]));
				$cad_buscar= array( '/\$/is', '/\%/is' );
				$cad_remplazo= array( '&#36;', '&#37;' );
				$descrip= preg_replace( $cad_buscar, $cad_remplazo, $descrip );

				$data_concept .= '<div id="cleandiv">
					<li class="cant">'. $cantidad. '</li>
					<li align="center" class="pu">'. $numid. '</li>
					<li class="concepto">'. $descrip. '</li>
					<li class="unidad">'. $clavprod. '</li>
					<li class="unidad">'. get_unidadesmedida( array('get_name_html', $unidades) ). '</li>
					<li align="right" class="pu">'. mbnumber_format($pprint, 2). '</li>
					<li align="right" class="pu">'. mbnumber_format($pd, 2). '</li>
					<li align="right" class="importe">'. mbnumber_format($cantidad*$precio_unitario,2). '</li>
					</div>';

				#if( $nc || $dsnc ) # para NC relacionamos la lista de conceptos de su factura
				#	{
				#	$datarel= init_conceptos( array("foliosrelacionados", "hash2arr"), $factura["ID_FACTURA"] );
				#
				#	foreach($datarel as $keyInfo=>$valInfo )
				#		{
				#		$tmpInfo= consultar_datos_general("FACTURACION", "ID='". $valInfo["id_factura"]. "' && ID_USUARIO='". $_SESSION["SUPERID"]. "'", "DESCRIPCION");
				#		$infoData= init_conceptos("hash2arr", $tmpInfo);
				#		$infoFacturaNC= $infoData["cantidad"]. ' | '. $infoData["concepto"]. '|'. mbnumber_format(($infoData["pu"]-$infoData["desc"]), 2, '.', ''). '|'. mbnumber_format(($infoData["cantidad"]*($infoData["pu"]-$infoData["desc"])), 2, '.', '');
				#		$data_concept .= '<div id="cleandiv">
				#			<li class="cant">&nbsp;</li>
				#			<li align="center" class="pu">&nbsp;</li>
				#			<li class="concepto">'. $infoFacturaNC. '</li>
				#			<li class="unidad">&nbsp;</li>
				#			<li align="right" class="pu">&nbsp;</li>
				#			<li align="right" class="pu">&nbsp;</li>
				#			<li align="right" class="importe">&nbsp;</li>
				#			</div>';
				#		}
				#	unset($infoFacturaNC, $datarel);
				#	}
			}
		}
		unset($precio_unitario, $cantidad, $descrip, $unidades);

		# extra impuestos
		if( !$factura["IMPUESTOS_LOCALES"] )
			$html= preg_replace( '/\[EXTRA_IMPUESTOS\]/', '', $html ); # subtotal
		else {
			$patron= '@\[descripcion\|([0-9a-zA-Z.,:+_\-/+\%\s#]{1,})\|valor\|([0-9.\%]{1,})\|tipo\|([0-9]{1,})\|porcentaje\|([0-9.\%]{1,})\|importe\|([0-9.\%]{1,})\]@';
			preg_match_all( $patron, $factura["IMPUESTOS_LOCALES"], $out ); # buscamos patron

			// print_r($out);

			if( !$out[0][0] )
				$html= preg_replace( '/\[EXTRA_IMPUESTOS\]/', '', $html ); # subtotal
			else { # patron encontrado
				$impuestosLocales='';
				foreach( $out[0] as $key=>$val ) {
					if( $out[2][$key] ) {
						$impuestosLocales .= '
						<div id="cleandiv">
							<li class="l">'. get_clienteprofile($out[1][$key], "dian_tributos_nombre", 0). ' '. mbnumber_format($out[4][$key], 2, '.', ''). '%</li>
							<li>'. mbnumber_format($out[2][$key], 2, '.', ','). '</li>
						</div>
						';
					}
				}
				$html= preg_replace( '/\[EXTRA_IMPUESTOS\]/', $impuestosLocales, $html ); # subtotal

				unset($aux);
			}

			foreach( $extraImp as $key=>$val ) {

			}
		}

		# descuento
		if( $desc[1] ) { # si hay descuento aplicable
			$d= '
				<div id="cleandiv">
					<li class="l">Deduccion</li>
					<li>'. mbnumber_format($desc[1], 2, '.', ','). '</li>
				</div>';
			$html= preg_replace( '/\[DESCUENTO\]/', $d, $html ); # subtotal
			/*
			$data_concept .= '<div id="cleandiv">
						<li class="cant"></li>
						<li class="concepto"></li>
						<li class="unidad"></li>
						<li align="right" class="pu"></li>
						<li align="right" class="importe"></li>
						</div>
						<div id="cleandiv">
							<li class="cant"></li>
							<li class="concepto">Descuento aplicable por: '. mbnumber_format($factura["DESCUENTO"], 2, '.', ','). ' MXN</li>
							<li class="unidad"></li>
							<li align="right" class="pu"></li>
							<li align="right" class="importe"></li>
						</div>';
			*/
		}
		else 
			$html= preg_replace( '/\[DESCUENTO\]/', '', $html ); # subtotal


		# si es aduana/comerializadora se calculan Tasas, IVAS y cargos de servicio
		if( (get_modulos(array($idUser, "isactive", 5)) || get_modulos(array($idUser, "isactive", 4))) && (!strcmp($frm, "aduana_comer") || !strcmp($frm, "aduana_gasto")) ) {
			$subtotal=0;
			$base=0;
			$total=0;
			$total_cli=0;
			# numero de pedimento 
			$html= preg_replace( '/\[PEDIMENTO\]/', $factura["PEDIMENTO"], $html ); # pedimento

			if( !strcmp($frm, "aduana_gasto") ) {
				# descripcion del pedimento
				$html= preg_replace( '/\[DETALLES\]/', $this->desproteger_cadena_src($factura["DESCRIPCION"] ? $factura["DESCRIPCION"]:'--'), $html ); # # descripcion
				$gastos_cli='';
							
				# verificando valores extras
				if( $factura["EXTRAS"] ) { # si hay campos extras
					$x= explode(",", $factura["EXTRAS"]);
					$extras=init_conceptos( "hash2arr", $x[0] ); # gastos aduana
					$extras_cli=init_conceptos( "hash2arr", $x[1]); # gastos cliente
				}
				else { # no hay valores 
					$extras=0;
					$extras_cli=0;
				}

				# extras de la aduana/comercializadora
				$gastos_adu='';
				if( $extras ) {
					foreach( $extras as $key=>$val ) {
						$clavprod= (!$val["cps"] ? '--':$this->getPanamaCatalogoCompras($val["cps"]));
						$gastos_adu .= '<div id="cleandiv">
							<li class="concepto">'. $this->desproteger_cadena_src($val["concepto"]). '</li>
							<li class="importe">'. $this->desproteger_cadena_src($clavprod). '</li>
							<li class="importe">'. mbnumber_format(($val["cantidad"]*$val["pu"]),2). '</li>
							</div>';
						unset($clavprod);
					}
					$extra_importes= sumgastos_importe( $extras, "normal"); # sumatoria de importes
					$html= preg_replace( '/\[GASTOS_ADUANA\]/', $gastos_adu, $html ); # gastos de aduana
					unset($ext, $r, $extras);
				}
					
				# valores de total de servicio
				$subtotal += $extra_importes; # suma de impuestos de aduana
				$base=$subtotal; # la base es el subtotal
				$cons_im= consultar_con( "IMPUESTOS", "ID_USUARIO='". proteger_cadena($idUser). "' && NOMBRE='". proteger_cadena($factura["IMPUESTO_NOMBRE"]). "'" );
				$bufimp= mysqli_fetch_array($cons_im); # obtenemos campos 
				
				# extras del cliente
				if( $extras_cli ) {
					foreach( $extras_cli as $key=>$val ) {
						$clavprod= (!$val["cps"] ? '--':$this->getPanamaCatalogoCompras($val["cps"]));
						$gastos_cli .= '<div id="cleandiv">
							<li class="concepto">'. desproteger_cadena($val["concepto"]). '</li>
							<li class="importe">'. $this->desproteger_cadena_src($clavprod). '</li>
							<li class="importe">'. mbnumber_format(($val["cantidad"]*$val["pu"]),2). '</li>
							</div>';
						unset($clavprod);
					}
					$html= preg_replace( '/\[GASTOS_CLIENTE\]/', $gastos_cli, $html ); # gastos de cliente
					$extra_importes2= sumgastos_importe( $extras_cli, "normal" ); # sumatoria
					unset($ext, $r, $extras_cli);
				}
				$total_cli= $extra_importes2;
				$total= $factura["TOTAL"];
				
				# imprimiendo valores
				#
				# iva
				$impuesto= $factura["IMPUESTO"];
				$html= preg_replace( '/\[IVA\]/', mbnumber_format( $impuesto, 2, '.', ','), $html ); # IVA

				# iva letra
				$imp_letra= explode( " ", $factura["IMPUESTO_NOMBRE"] );
				$html= preg_replace( '/\[IVA_P\]/', $imp_letra[1], $html ); # IVA Porcentaje
				
				$html= preg_replace( '/\[SUBTOTAL\]/', mbnumber_format($subtotal, 2, '.', ','), $html ); # subtotal
				$html= preg_replace( '/\[TOTAL_SERVICIOS\]/', mbnumber_format( ($subtotal+$impuesto), 2, '.', ','), $html ); # subtotal
				$html= preg_replace( '/\[TOTAL\]/', mbnumber_format( $total, 2, '.', ','), $html ); # subtotal
				unset($x_sub);
				$html= preg_replace( '/\[TOTAL_GASTOS\]/', mbnumber_format( $total_cli, 2, '.', ','), $html ); # total gastos
				$html= preg_replace( '/\[TOTAL_NETO\]/', mbnumber_format( $total, 2, '.', ','), $html ); # total neto
				$letratotal= convertir_a_letras( $total, $factura["MONEDA_TIPO"] );
				$html= preg_replace( '/\[CANTIDAD_LETRA\]/', $letratotal, $html ); # total neto
			}
			else if( !strcmp($frm, "aduana_comer" ) ) {
				for($i=0; $i<5; $i++ ) {
					$data_concept .= '<div id="cleandiv">
							<li class="cant"></li>
							<li align="right" class="pu"></li>
							<li align="right" class="importe"></li>
							</div>';
				}

				# impuestos de comercializados / aduana
				$p= '16';
				if( $factura["EXTRAS"] ) { # si hay campos extras
					$extras= init_conceptos("hash2arr", $factura["EXTRAS"]);
					foreach( $extras as $key=>$val ) {
						$clavprod= (!$val["cps"] ? '--':$this->getPanamaCatalogoCompras($val["cps"]));
						$porc= ($val["impuesto"] ? 16:0);
						$imp= ($val["cantidad"]*$val["pu"]);
						$tasas[$porc] += $imp;

						$data_concept .= '<div id="cleandiv">
							<li class="cant">'. $this->desproteger_cadena_src($val["concepto"]). '</li>
							<li class="unidad">'. $clavprod. '</li>
							<li align="right" class="pu">'. $this->desproteger_cadena_src($porc). '</li>
							<li align="right" class="importe">'. mbnumber_format($imp,2, '.', ','). '</li>
							</div>';
					}
					unset($ext, $r);
				}

				$tasa_neto= '';
				$total=$factura["TOTAL"];
				$subtotal=$factura["SUBTOTAL"];
				$totalsrv= sumgastos_importe( $extras, "normal" );

				foreach( $tasas as $a=>$b ) {
					$tasa_neto .= '
					<div class="cleandiv">
						<li class="l">Tasa '. $a. '%</li>
						<li>'. mbnumber_format($b, 2, '.', ','). '</li>
					</div>';
				}

				$tasa_neto .= '
					<div class="cleandiv">
						<li class="l">SUBTOTAL</li>
						<li>'. mbnumber_format($subtotal, 2, '.', ','). '</li>
					</div>
					<div class="cleandiv">
						<li class="l">I.V.A. '. $p. '%</li>
						<li>'. mbnumber_format($factura["IMPUESTO"], 2, '.', ','). '</li>
					</div>
					<div class="cleandiv">
						<li class="l">TOTAL</li>
						<li>'. mbnumber_format($total, 2, '.', ','). '</li>
					</div>
					<div class="cleandiv">
						<li class="l">TOTAL SERVICIOS</li>
						<li>'. mbnumber_format($totalsrv+$factura["IMPUESTO"], 2, '.', ','). '</li>
					</div>';

				unset( $preval, $aduana, $comercia, $dta, $importe, $p );

				# cantidad con letra
				$letratotal= convertir_a_letras( $total, $factura["MONEDA_TIPO"] );
				$html= preg_replace( '/\[CANTIDAD_LETRA\]/', $letratotal, $html ); # IVA
				
				# neto tasas
				$html= preg_replace( '/\[TASAS_NETO\]/', $tasa_neto, $html ); # total neto de TASAS
			}

			# comercio exterior
			if( $factura["COMERCIO_EXTERIOR"] ) { # si hay etiquetq
				$comerextra_data= array(
					"r"=>$factura["COMEREXTRA_RECEPTOR"], 
					"d"=>get_systeminfo( array("sys"=>"factura", "op"=>"comerext", "cmd"=>"data2arr", "data"=>$factura["COMEREXTRA_DESTINATARIOS"])), 
					"p"=>get_systeminfo( array("sys"=>"factura", "op"=>"comerext", "cmd"=>"data2arr", "data"=>$factura["COMEREXTRA_PROPIETARIOS"])), 
					"m"=>get_systeminfo( array("sys"=>"factura", "op"=>"spcomerext", "cmd"=>"data2arr", "data"=>$factura["COMEREXTRA_MERCANCIAS"])), 
					"o"=>get_systeminfo( array("sys"=>"factura", "op"=>"comerext", "cmd"=>"data2arr", "data"=>$factura["COMEREXTRA_OTROS"]))
					);

				$cmext= '
					<div class="cleandiv">
						<li style="width:100%;margin-top:16px;" class="titulo negro txtblanco centroalign">COMERCIO EXTERIOR</li>
						<li align="center" class="pu titulo negro txtblanco">cant</li>
						<li align="center" class="importe titulo negro txtblanco">NI</li>
						<li align="center" class="importe titulo negro txtblanco">Fraccion</li>
						<li align="center" class="importe titulo negro txtblanco">mercancias</li>
						<li align="center" class="importe titulo negro txtblanco">unidad</li>
						<li align="center" class="importe titulo negro txtblanco">pu</li>
						<li align="center" class="importe titulo negro txtblanco">importe usd</li>
					</div>
					';

				foreach( $comerextra_data["m"] as $key=>$val ) {
					$cmtxtunid= ( (substr($val["unidad"], 0, -1)==0) ? substr($val["unidad"], -1):$val["unidad"]);
					$cmext .= '
					<div id="cleandiv">
						<li align="center" class="pu txtnegro">'. desproteger_cadena($val["cantidad"]). '</li>
						<li align="center" class="importe txtnegro">'. $this->desproteger_cadena_src($val["serie"]). '</li>
						<li align="center" class="importe txtnegro">'. $this->desproteger_cadena_src(get_clienteprofile( $val["concepto"], "aranceles_clave", 0 )). '</li>
						<li align="center" class="importe txtnegro">'. $this->desproteger_cadena_src(get_clienteprofile( $val["concepto"], "aranceles_nombre", 0 )). '</li>
						<li align="center" class="importe txtnegro">'. $this->desproteger_cadena_src(get_unidadesmedida( array("get_name", $cmtxtunid, "comercio_exterior") )). '</li>
						<li align="center" class="importe txtnegro">'. mbnumber_format($val["pu"], 2, '.', ','). '</li>
						<li align="center" class="importe txtnegro">'. mbnumber_format(($val["cantidad"]*$val["pu"]), 2, '.', ''). '</li>
					</div>
						';
				}			
				$html= preg_replace( '/\[COMEXTERIOR\]/', $cmext, $html ); # IVA
			}
			else 	
				$html= preg_replace( '/\[COMEXTERIOR\]/', '', $html ); # IVA
		}
		else { # calculos: normal, arrenda, transporte
			# inicializador
			if( get_modulos(array($idUser, "isactive", 3)) && !strcmp($frm, "normal") ) { # factura comun
				$html= preg_replace( '/\[IVA_RET\]/', '', $html ); # iva ret
				$html= preg_replace( '/\[ISR_RET\]/', '', $html ); # isr ret
			}

			# procentaje IVA impreso
			$xivaprint=1;
			if( $xivaprint ) { # si se requiere imprimir
				if( !strcmp($cliente["REGIMEN_FISCAL"], "30") && !strcmp(consultar_datos_general( "USUARIOS", "ID='". $idUser. "'", "REGIMEN_FISCAL"), "9") )
					$html= preg_replace( '/\[IVA_P\]/', 'NA', $html ); # iva impreso el porcentaje
				else {
					$ivaporcentaje= $factura["IMPUESTO_FORMULA"];
					$html= preg_replace( '/\[IVA_P\]/', $ivaporcentaje, $html ); # iva impreso el porcentaje
					unset($ivaporcentaje);
				}
			}
			unset($xivaprint);
				
			# IMPUESTO IVA, ISR o IEPS
			if( $factura["IMPUESTO"] && strcmp($factura["IMPUESTO"], "0.00") ) { # si hay algun impuesto a cobrar
				# honorarios y persona fisica (causante menor), solo causa ISR
				if( get_modulos(array($idUser, "isactive", 2)) && !strcmp($frm, "honorarios") && !strcmp($cliente["PERFIL"], "1")  ) {
					$html= preg_replace( '/\[IVA\]/', mbnumber_format( (!$descuento ? $factura["IMPUESTO"]:($factura["IMPUESTO"]*$descuento)), 2, '.', ','), $html ); # iva
					$html= preg_replace( '/\[ISR_RET\]/', '', $html ); # isr ret
					$html= preg_replace( '/\[IVA_RET\]/', '', $html ); # iva ret
				}
				else # normal, arrenda, transporte, aduana y comercializadora -- causan IVA
					$html= preg_replace( '/\[IVA\]/', number_format(($factura["IMPUESTO"] ? (!$descuento ? $factura["IMPUESTO"]:($factura["IMPUESTO"]*$descuento)):0), 2), $html ); # iva
			}
			else { # son servicios sin retencion
				$html= preg_replace( '/\[IVA_RET\]/', mbnumber_format( ($factura["RET_IVA"] ? $factura["RET_IVA"]:0), 2, '.', ','), $html ); # iva ret
				$html= preg_replace( '/\[ISR_RET\]/', mbnumber_format( ($factura["RET_ISR"] ? $factura["RET_ISR"]:0), 2, '.', ','), $html ); # iva ret
			}

			# imprecion del TOTAL
			# factura normal a gobierno con retencion del iva
			if( $factura["RET_IVA"] && get_modulos(array($idUser, "isactive", 1)) && !strcmp($cliente["REGIMEN_FISCAL"], "20") ) {
				$totalgobret= $factura["TOTAL"]; # total nuevo
				$html= preg_replace( '/\[TOTAL\]/', number_format($totalgobret, 2), $html ); # total
				unset($totalgobret);
			}
			else if( (get_modulos(array($idUser, "isactive", 2)) || get_modulos(array($idUser, "isactive", 3))) && (!strcmp($frm, "arrenda") || !strcmp($frm, "honorarios")) ) { # arrendamiento y honorarios
				# si el cliente es causante mayor ponemos retenciones
				if( !strcmp( consultar_datos_general("CLIENTES", "ID='". proteger_cadena($factura["ID_CLIENTE"]). "'", "PERFIL"), "2") ) {
					$html= preg_replace( '/\[ISR_RET\]/', mbnumber_format( (($factura["SUBTOTAL"]*10)/100), 2, '.', ','), $html ); # isr ret

					# a gobierno
					if( !strcmp($cliente["REGIMEN_FISCAL"], "20") )
						$html= preg_replace( '/\[TOTAL\]/', mbnumber_format( $factura["TOTAL"], 2, '.', ','), $html ); # total
					else {		# persona moral
						$html= preg_replace( '/\[IVA_RET\]/', mbnumber_format( (($factura["IMPUESTO"]*66.666)/100), 2, '.', ','), $html ); # iva ret
						$html= preg_replace( '/\[TOTAL\]/', mbnumber_format( $factura["TOTAL"], 2, '.', ','), $html ); # total
					}
				}
				else	# imprimimos el "subtotal" como total por disposicion legal
					$html= preg_replace( '/\[TOTAL\]/', mbnumber_format($factura["TOTAL"], 2, '.', ','), $html ); # total
			}
			else if( get_modulos(array($idUser, "isactive", 12)) && !strcmp($frm, "transporte") ) { # transportista
				$html= preg_replace( '/\[IVA_RET\]/', mbnumber_format( $factura["RET_IVA"], 2, '.', ','), $html ); # iva ret
				$html= preg_replace( '/\[TOTAL\]/', mbnumber_format( $factura["TOTAL"], 2, '.', ','), $html ); # total
				$html= preg_replace( '/\[ISR_RET\]/', '', $html ); # isr ret
			}
			else		# factura normal
				$html= preg_replace( '/\[TOTAL\]/', mbnumber_format( (!$descuento ? $factura["TOTAL"]:($factura["TOTAL"]*$descuento)), 2, '.', ','), $html ); # total

			# arrendatarios y honorarios -- calculo de total
			if( (get_modulos(array($idUser, "isactive", 2)) || get_modulos(array($idUser, "isactive", 3))) && (!strcmp($frm, "arrenda") || !strcmp($frm, "honorarios")) ) {
				# causante mayor
				if( !strcmp( consultar_datos_general("CLIENTES", "ID='". proteger_cadena($factura["ID_CLIENTE"]). "'", "PERFIL"), "2") ) {
					# a gobierno
					if( !strcmp($cliente["REGIMEN_FISCAL"], "20") )
						$tl= mbnumber_format(($factura["TOTAL"]+(($factura["SUBTOTAL"]*10)/100)), 2, '.', '');
					else		# persona moral
						$tl= mbnumber_format($factura["TOTAL"], 2, '.', '' );
				}
				else # causante menor
					$tl= mbnumber_format($factura["TOTAL"], 2, '.', '' );
			}
			# factura normal a gobierno con retencion del iva
			else	
				$tl= mbnumber_format((!$descuento ? $factura["TOTAL"]:($factura["TOTAL"]*$descuento)), 2, '.', '' );

			# prevencion para COTIZACIONES
			$html= preg_replace( '/\[IVA\]/', '0.00', $html ); # iva ret
			$html= preg_replace( '/\[IVA_RET\]/', '0.00', $html ); # iva ret
			$html= preg_replace( '/\[ISR_RET\]/', '0.00', $html ); # isr ret

			# COMPLEMENTOS
			#
			# IMPUESTOS LOCALES
			# 

			#if( !$factura["IMPUESTOS_LOCALES"] ) # si no hay impuestos locales
				$html= preg_replace( '/\[COMPLEMENTOS\]/', '', $html ); # dejamos vacio
			#else { # sacamos los impuestos locales
			#	$implocales_info='';
			#	$implocales= get_systeminfo(array( "sys"=>"factura", "op"=>"implocal", "cmd"=>"data2arr", "data"=>$factura["IMPUESTOS_LOCALES"] )); # consultamos de data a array
			#
			#	foreach( $implocales as $key=>$val ) {
			#		$implocal_val= $val["porcentaje"];
			#		$implocal_imp= $val["valor"];
			#
			#		$implocales_info .= '
			#			<div id="cleandiv">
			#				<li class="l">'. desproteger_cadena($val["descripcion"]). ' '. ( !strcmp($val["tipo"], "1" ) ? 'Ret.':'Tras.'). ' '. $this->desproteger_cadena_src($implocal_val). '</li>
			#				<li>'. mbnumber_format($implocal_imp, 2, '.', ','). '</li>
			#			</div>';
			#		unset($implocal_imp, $implocal_val);
			#	}
			#
			#	$html= preg_replace( '/\[COMPLEMENTOS\]/', $implocales_info, $html ); # agregamos
			#	unset($implocales);
			#}

			$letratotal= convertir_a_letras( $tl, $factura["MONEDA_TIPO"] );
			$html= preg_replace( '/\[CANTIDAD_LETRA\]/', $letratotal, $html ); # cantidad con letra
			unset($tl);
		}

		# nota de credito
		if( $nc || $dsnc ) {
			$data_amparo='';
			if( $factura["ID_FACTURA"] ) {
				$datarel= init_conceptos( array("foliosrelacionados", "hash2arr"), $factura["ID_FACTURA"] );

				foreach($datarel as $key=>$val ) {
					$data_amparo .= '
					<li style="width:60%;" class="concepto blanco txtnegro centroalign">'. $val["uuid"]. '</li>
					<li style="width:40%;" class="blanco txtnegro centroalign">'. date( "d/m/Y", consultar_datos_general( "FACTURACION", "ID_USUARIO='". $idUser. "' && ID='". $val["id_factura"]. "'", "FECHA")). '</li>
					';
				}
				unset($datarel);
			}

			$html= preg_replace( '/\[CONCEPTOS_AMPARO\]/', $data_amparo, $html ); # conceptos de factura
			unset($data_amparo);

			# tipo de operacion
			$html= preg_replace( '/\[TIPO_OPERACION\]/', get_clienteprofile($factura["TIPO_OPERACION"], "dian_tipo_operacion_print", 0), $html ); # conceptos de factura
		}

		#
		# documentos relacionados
		#
		$doctosrel='';
		if( $factura["DOCTOSREL_FACTURAS"] || $factura["DOCTOSREL_CANCELACIONES"] || $factura["DOCTOSREL_NOTASCREDITO"] ) {
			$doctosrel_arr= get_systeminfo(array( "sys"=>"factura", "op"=>"doctosrel", "cmd"=>"data2arr", "data"=>$factura["ID"])); # inicializa
			$doctosrel .= '
			<div id="receptor">
				<h1 class="titulo negro txtblanco">Documentos Relacionados</h1>
					<div class="in">
						<ul>
				';

			# factura
			if( count($doctosrel_arr["doctos"]["factura"]) ) {
				foreach( $doctosrel_arr["doctos"]["factura"] as $key=>$val )
					$doctosrel .= '<div id="cleandiv"><li class="cp">'. $val["nombre"]. '</li><li class="cp">'. strtoupper($val["uuid"]). '</li><li class="cp rightalign">'. mbnumber_format($val["monto"], 2, '.', ','). '</li></div>';
			}
			# cancelaciones
			if( count($doctosrel_arr["doctos"]["cancelaciones"]) ) {
				foreach( $doctosrel_arr["doctos"]["cancelaciones"] as $key=>$val )
					$doctosrel .= '<div id="cleandiv"><li class="cp">Cancelada '. $val["nombre"]. '</li><li class="cp">'. strtoupper($val["uuid"]). '</li><li class="cp rightalign">'. mbnumber_format($val["monto"], 2, '.', ','). '</li></div>';
			}
			# nota de credito
			if( count($doctosrel_arr["doctos"]["notacredito"]) ) {
				foreach( $doctosrel_arr["doctos"]["notacredito"] as $key=>$val )
					$doctosrel .= '<div id="cleandiv"><li class="cp">'. $val["nombre"]. '</li><li class="cp">'. strtoupper($val["uuid"]). '</li><li class="cp rightalign">'. mbnumber_format($val["monto"], 2, '.', ','). '</li></div>';
			}
			$doctosrel .= '
						</ul>
					</div>
				</div>';
		}

		$html= preg_replace( '/\[DOCUMENTOSRELACIONADOS\]/', $doctosrel, $html ); # documentos relacionados

		$html= preg_replace( '/\[SUBTOTAL\]/', mbnumber_format($factura["SUBTOTAL"], 2), $html ); # subtotal	
		$html= preg_replace( '/\[CONCEPTOS\]/', $data_concept, $html ); # conceptos de factura
		unset($frm, $x_sub, $y, $datos);

		# predial - arrendatarios
		$html= preg_replace( '/\[PREDIAL\]/', ($factura["PREDIAL"] ? $factura["PREDIAL"]:''), $html ); # metodo de pago

		# metodo de pago
		# 1 --> efectivo, 2 --> no identificado, 3 --> transferencia, 4 --> tarjeta debito/credito/servicio, 5 --> cheque nominativo
		#
		# si se requieren impresos
		# 
		$metodopago= get_clienteprofile( $factura["METODO_PAGO"], "metodopago", 0 ); # texto del metodo de pago
		#$metodopago .= (strcmp($factura["METODO_PAGO"], "10") ? ', Cuenta: '. desproteger_cadena($factura["METODO_PAGO_DIGITOS"]):'');

		if( $metodopago ) # si hay metodo de pago
			$html= preg_replace( '/\[METODO_PAGO\]/', $metodopago, $html ); # metodo de pago
		unset( $metodopago);

		$fPago='';
		if( date("Y", $factura["FECHA_VENCIMIENTO"])<date("Y", time()) )
			$fPago= 'Contado';
		else if( date("Y", $factura["FECHA_VENCIMIENTO"])==date("Y", time()) ) {
			if( date("m", $factura["FECHA_VENCIMIENTO"])<date("m", time()) )
				$fPago= 'Contado';
			else if( date("m", $factura["FECHA_VENCIMIENTO"])==date("m", time()) ) {
				if( date("d", $factura["FECHA_VENCIMIENTO"])<date("d", time()) )
					$fPago= 'Contado';
				else if( date("d", $factura["FECHA_VENCIMIENTO"])==date("d", time()) )
					$fPago= 'Contado';
				else
					$fPago= 'Cr&eacute;dito';
			}
			else
				$fPago= 'Cr&eacute;dito';
		}
		else
			$fPago= 'Cr&eacute;dito';
		
		#$html= preg_replace( '/\[FORMA_PAGO\]/', (!$factura["FORMA_PAGO"] ? '':($factura["FECHA_VENCIMIENTO"] ? (!strcmp(date("d/m/Y", $factura["FECHA_VENCIMIENTO"]), date("d/m/Y", time())) ? "Contado":"Cr&eacute;dito"):($factura["FORMA_PAGO"]. ' - '. get_clienteprofile($factura["FORMA_PAGO"], "forma_pago", 0))) ), $html ); # metodo de pago
		$html= preg_replace( '/\[FORMA_PAGO\]/', (!$factura["FORMA_PAGO"] ? '':($factura["FECHA_VENCIMIENTO"] ? $fPago:($factura["FORMA_PAGO"]. ' - '. get_clienteprofile($factura["FORMA_PAGO"], "forma_pago", 0))) ), $html ); # metodo de pago

		$html= preg_replace( '/\[USO_CFD\]/', ($factura["USO_CFD"] ? get_clienteprofile( $factura["USO_CFD"], "usocfd", 0 ):'G03'), $html ); # usocfdi

		# leyendas fiscales
		$html= preg_replace( '/\[LEYENDA_FISCAL\]/', ($factura["LEYENDAS_FISCALES"] ? $factura["LEYENDAS_FISCALES"]:''), $html );

		#
		# tipo de moneda
		#
		$html= preg_replace( '/\[MONEDA_TIPO\]/', get_clienteprofile( $factura["MONEDA_TIPO"], "moneda", 0 ), $html );
		$html= preg_replace( '/\[MONEDA_VALOR\]/', $factura["MONEDA_VALOR"], $html );
		$html= preg_replace( '/\[CONDICION_PAGO\]/', desproteger_cadena(get_clienteprofile($factura["CONDICION_PAGO"], "condicion_pago", 0) ? get_clienteprofile($factura["CONDICION_PAGO"], "condicion_pago", 0):'Otros'), $html ); # usocfdi
		
		#
		# orden de compra, poliza y referencia
		#
		if( $factura["OC"] && strcmp($factura["OC"], "Orden de Compra") )
			$html= preg_replace( '/\[CLIENTE_PO\]/', desproteger_cadena($factura["OC"]), $html ); # orden de compra
		else		$html= preg_replace( '/\[CLIENTE_PO\]/', '', $html ); # orden de compra
		if( $factura["POLIZA"] && strcmp($factura["POLIZA"], "Poliza de Servicio") )
			$html= preg_replace( '/\[CLIENTE_POLIZA\]/', desproteger_cadena($factura["POLIZA"]), $html ); # poliza
		else		$html= preg_replace( '/\[CLIENTE_POLIZA\]/', '', $html ); # poliza
		if( $factura["REF"] && strcmp($factura["REF"], "Referencia") )
			$html= preg_replace( '/\[CLIENTE_REFERENCIA\]/', desproteger_cadena($factura["REF"]), $html ); # referencia
		else		$html= preg_replace( '/\[CLIENTE_REFERENCIA\]/', '', $html ); # referencia

		$html= preg_replace( '/\[FECHA_VENCIMIENTO\]/', ($factura["FECHA_VENCIMIENTO"] ? date( "d/m/Y", $factura["FECHA_VENCIMIENTO"]):"---"), $html );

		# si expide Facturas Digitales 	
		if( !strcmp(consultar_datos_general("USUARIOS", "ID='". proteger_cadena($idUser). "'", "SERVICIO"), "fd") ) {
			$html= preg_replace( '/\[SERIE_CSD\]/', $this->getCert(), $html ); # serie csd
			$html= preg_replace( '/\[TIMBRE_FISCAL\]/', $this->getUUID(), $html ); # folio fiscal UUID
			$html= preg_replace( '/\[SELLO_CFDI\]/', $this->getSSC(), $html ); # sello CFDi
			$html= preg_replace( '/\[SELLO_SAT\]/', base64_encode(json_encode($this->getUUID())), $html ); # sello SAT
			# $html= preg_replace( '/\[CSD_SAT\]/', $timbredata["TimbreFiscalDigital"]["NoCertificadoSAT"], $html ); # num certificado SAT
			$html= preg_replace( '/\[FECHA_TIMBRE\]/', $this->fecha, $html ); # fecha timbre fiscal
			# $html= preg_replace( '/\[CO\]/', $sello, $html ); # cadena original
			$html= preg_replace( '/\[RESOLUCION\]/', $gestorFolio["NUM_APROBACION"], $html );
			$html= preg_replace( '/\[RESOLUCION_FECHA\]/', date("Y-m-d", $gestorFolio["FECHA_INICIO"]). ' - '.date("Y-m-d", $gestorFolio["FECHA_VENCIMIENTO"]), $html );
			$html= preg_replace( '/\[RANGO_FOLIOS\]/', ($gestorFolio["FOLIO_SERIE"]. ' '. $gestorFolio["FOLIO_INICIO"]. ' - '. $gestorFolio["FOLIO_FIN"]), $html );
			
			# url descarga xml y pdf
			if( $factura["SHORT_URL"] )
				$html= preg_replace( '/\[URL\]/', $factura["SHORT_URL"], $html ); # orden de compra
			# url validacion DIAN
			$urlDianSrc= $this->getDIANDocUrl();
			$urlDian= src_mx($urlDianSrc);
			$html= preg_replace( '/\[URL_VALIDA_DIAN\]/', ($urlDian ? $urlDian:$urlDianSrc), $html ); # orden de compra
			
			unset($timbredata, $xml, $filet, $rwf, $urlDian, $urlDianSrc);
		}

		# limpiamos variables
		unset($buf, $factura, $cliente);
		$fp= fopen( $html_fact, "w" ); # creamos archivo html
		fwrite($fp, $html );	# guardamos datos html
		fclose($fp);	# cerramos stream
		return 1;
	}

	/**
	* corta un string
	* 
	* @param string $a numero
	* @param string $pos numero de posiciones deseadas
	* @return string nuevo valor
	*/
	public function cutString($a=NULL, $pos=NULL) {
		if( !$a || !$pos )
			return 0;
		else {
			if( strlen($a)==$pos ) { # misma cantidad de posiciones
				return $a; # devolvemos
			}
			else if( strlen($a)>$pos ) { # supera las posiciones
				return substr($a, 0, $pos); # devolvemos
			}
			else { # es menor
				return $this->setCeros($a, $pos);
			}
		}
	}

	/**
	* valida un telefono
	* 
	* @param string $a numero
	* @return boolen verdadero o falso
	*/
	public function isValidTelefono($a=NULL) {
		$tmpPhone= $a;
		if( strstr($a, ",") ) { // hay varios delimitados por coma
			$x= explode(",", $a);
			$tmpPhone= $x[0];
		}

		$phone= str_replace(array(" ", ".", "-", "(", ")"), "", $tmpPhone);

		if( strlen($phone)<7 || strlen($phone)>12 )
			return false;
		else
			return true;
	}

	public function getFixedPhone($a=NULL) {
		$tmpPhone= $a;
		if( strstr($a, ",") ) { // hay varios delimitados por coma
			$x= explode(",", $a);
			$tmpPhone= $x[0];
		}

		return $tmpPhone;
	}

	/**
	* valida un telefono
	* 
	* @param string $a el telefono
	* @return boolen verdadero o falso
	*/
	public function getValidTelefono($a=NULL) {
		if( !$this->isValidTelefono($a) )
			return 0;
		else {
			$phone= $this->getFixedPhone($a);
			$phone= str_replace(array(" ", ".", "-", "(", ")"), "", $phone);
			$invertPhone='';
			$newPhone='';
			$j=0;

			for( $i=strlen($phone); $i>0; $i-- ) {
				if( !($j%4) && isset($phone[$i]) ) 
					$invertPhone .= '-';
				$invertPhone .= (isset($phone[$i-1]) ? $phone[$i-1]:'');
				$j++;
			}

			for( $i=strlen($invertPhone); $i>0; $i-- ) {
				$newPhone .= (isset($invertPhone[$i-1]) ? $invertPhone[$i-1]:'');
			}

			return $newPhone;
		}
	}

	/**
	* ordenamiento apartir de la muestra de variable
	*
	* @param array $s arreglo a recorrer
	* @param string $m muestra de la variable a tomar como base
	*
	* @return array arreglo ordenado
	*/
	public function orBobImp($a=NULL, $m=NULL) {
		if( !is_array($a) || !$m )	return 0;
		else {
			foreach( $a as $key=>$val) {
				$impuesto[$key] = $val['impuesto'];
				$clave[$key] = $val[$m];
			}
			array_multisort($clave, SORT_ASC, $impuesto, SORT_DESC, $a);
			return $a;
		}
	}

	/**
	* convierte conceptor de factura
	*
	* @param string texto plano de los datos a leer
	* @param string comando de conversion
	*
	* @return retorna un valor cuando el servicio asi lo requiera, alternativamente retorna siempre 0
	*/
	public function initConceptosFactura($data=NULL, $modo=NULL) {
		$r=0;

		if( is_array($modo) ) {
			if(!strcmp($modo["op"], "impuestos_extra")) { # impuestos extras por concepto
				if( !strcmp($modo["cmd"], "arr2data") ) { # de hash (base) a array (memoria))
					$r='';
					foreach( $data as $key=>$val ) {
						if( is_array($val) ) { # si es arreglo
							$r .= '['. $val["id"]. '|'. urlencode($val["nombre"]). '|'. $val["tipo"]. '|'. $val["tasa"]. '|'. $val["importe"]. '|'. $val["impuesto"]. ']';
						}
					}
				}
				else if( !strcmp($modo["cmd"], "data2arr") ) { # de array a hash 
					$patron= '@\[([0-9a-zA-Z]{1,})\|([0-9a-zA-Z.,:%#+_\-/+]{1,})\|([0-9a-zA-Z.]{1,})\|([0-9.\%]{1,})\|([0-9.]{1,})\|([0-9.]{1,})\]@';
					preg_match_all( $patron, $data, $out ); # buscamos patron

					if( !$out[0][0] )
						$r=array();
					else { # patron encontrado
						$aux= array();
						foreach( $out[0] as $key=>$val ) {
							$aux[]= array( "id"=>$out[1][$key], "nombre"=>urldecode($out[2][$key]), "tipo"=>$out[3][$key], "tasa"=>$out[4][$key], "importe"=>$out[5][$key], "impuesto"=>$out[6][$key] ); # tomamos impuesto extra del concepto
						}
						$r= $this->orBobImp($aux, "tipo"); # arreglo formado
						# $r= $aux; # arreglo formado
						unset($aux);
					}
				}
			}
			else if(!strcmp($modo["op"], "conceptos")) { # impuestos extras por concepto
				$r=array();
				$patron= '@\[([0-9a-zA-Z]{1,})\|([0-9.]{1,})\|([0-9a-zA-Z_\-]{1,})\|([0-9a-zA-Z.,:%#+_\-/+]{1,})\|([0-9.,]{1,})\|([0-9a-zA-Z.,:%#+_\-/+]{1,})\|([0-9.,]{1,})\|([0-9a-zA-Z.,:%#+_\-/+]{1,})\|([0-9.]{1,})\|([0-9.]{1,})\|([0-9.]{1,})\|([0-9.]{1,})\|([0-9a-zA-Z.,:%#+_\-/+]{1,})\]@';
				preg_match_all($patron, $data, $out);

				foreach( $out[0] as $key=>$val ) {
					if( strstr(urldecode($out[8][$key]), ",") ) { # catalogo especial cliente
						$x= explode(",", urldecode($out[8][$key]));
						$cps= array( "clave"=>$x[0], "tipo"=>$x[1], "codigo"=>$x[2] );
					}
					else # normal 
						$cps= $out[8][$key];
					unset($x);

					$otros=array();
					if( strstr(urldecode($out[13][$key]), ",") ) { # catalogo especial cliente
						$x= explode(",", urldecode($out[13][$key]));

						foreach( $x as $k ) {
							$otros[]= urldecode($k);
						}
					}
					else # normal 
						$otros[]= $out[13][$key];

					$r[$out[1][$key]]= array(
						"cantidad"=>$out[2][$key], 
						"unidad"=>$out[3][$key], 
						"concepto"=>urldecode($out[4][$key]), 
						"pu"=>$out[5][$key], 
						"ni"=>urldecode($out[6][$key]), 
						"desc"=>$out[7][$key], 
						"cps"=>$cps, 
						"impuesto"=>$out[9][$key], 
						"importe"=>$out[10][$key], 
						"codigo_compra"=>$out[11][$key], 
						"fecha_compra"=>$out[12][$key], 
						"otros"=>$otros
					);
					unset($cps);
				}
			}
		}
		return $r;
	}

	/**
	* guarda los XMLs de factura y autorizacion
	*/
	public function guardarXML($xml=NULL, $xmlAuth=NULL) {
		if( !$xml || !$xmlAuth )
			return 0;
		else {
			$r= $this->getRespuesta();
			$fileXml= $xml;
			$fileXmlAuth= $xmlAuth;
			$fp= fopen($fileXml, "w");
			fwrite($fp, base64_decode($r["xml"]));
			fclose($fp);
			unset($fp);
			$fp= fopen($fileXmlAuth, "w");
			fwrite($fp, base64_decode($r["xml_aut"]));
			fclose($fp);
			unset($fp);

			$this->setUUID($r["cufe"]); # guarda uuid
			return $this->getUUID();
		}
	}

	/**
	* clase principal
	*/
	public function __construct($sandbox=false) {
		$this->setSandbox($sandbox); // por defecto apaga sandbox
	}
}
?>